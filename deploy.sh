
rm -rf ./app
npm run postinstall
NODE_ENV=$NODE_ENV webpack --config webpack.production.config.js
curl -sL http://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-x64.tar.gz | tar -xzv

mv node-v$NODE_VERSION-linux-x64 ./app/runtime 

echo '{
  "name": "dent",
  "version": "0.0.1",
  "engines": {
    "node": "14.17.3",
    "npm": "6.14.13"
  },
  "scripts": {},
  "dependencies": { "@grpc/proto-loader": "^0.6.4" }
}' > ./app/package.json

cd ./app
npm i 
cd ../

tar czfv slug.tgz ./app ./Procfile


http_response=$(curl -X POST https://api.heroku.com/apps/dental-box-server/slugs \
-H 'Content-Type: application/json' \
-H 'Accept: application/vnd.heroku+json; version=3' \
-H  "Authorization: Bearer ${HEROKU_TOKEN}" \
-d '{"process_types":{"web":"runtime/bin/node server/app.js"}}'
)

tmpUrl=$(echo ${http_response} | jq '.blob.url')
deploy_url="${tmpUrl%\"}"
deploy_url="${deploy_url#\"}"

tmpId=$(echo ${http_response} | jq '.id')
id="${tmpId%\"}"
id="${id#\"}"

echo $id


deploy_response=$(curl -X PUT $deploy_url \
-H "Content-Type:" \
--data-binary @slug.tgz 
)

echo $deploy_response

release=$(curl -X POST https://api.heroku.com/apps/dental-box-server/releases \
-H "Accept: application/vnd.heroku+json; version=3" \
-H "Content-Type: application/json" \
-H  "Authorization: Bearer ${HEROKU_TOKEN}" \
-d "{\"slug\": \"${id}\"}")

echo ${release}

rm -rf  ./slug.tgz ./app


