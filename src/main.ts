import { NestFactory } from '@nestjs/core';
import { AppModule } from 'app/app.module';
import { ExceptionFiltering } from 'app/errors/filters/exception.handler';
import { ValidationFilter } from 'app/errors/filters/validation.filter';
import { HttpGlobalInterceptor } from 'app/interceptor';
import {
  cabinetGrpcOptions,
  cardGrpcOptions,
  diseaseGrpcOptions,
  patientGrpcOptions,
  userGrpcOptions,
  chartGrpcOptions,
  scheduleGrpcOptions,
  periodentalGrpcOptions,
  statisticGrpcOptions,
  authGrpcOptions,
} from 'app/options_grpc';

const runApp = async () => {
  const app = await NestFactory.create(AppModule, { logger: ['error'] });

  app.connectMicroservice(userGrpcOptions);
  app.connectMicroservice(cabinetGrpcOptions);
  app.connectMicroservice(patientGrpcOptions);
  app.connectMicroservice(cardGrpcOptions);
  app.connectMicroservice(diseaseGrpcOptions);
  app.connectMicroservice(chartGrpcOptions);
  app.connectMicroservice(scheduleGrpcOptions);
  app.connectMicroservice(periodentalGrpcOptions);
  app.connectMicroservice(statisticGrpcOptions);
  app.connectMicroservice(authGrpcOptions);

  app.useGlobalInterceptors(new HttpGlobalInterceptor());
  app.useGlobalFilters(new ExceptionFiltering());

  app.useGlobalPipes(new ValidationFilter());

  await app.startAllMicroservices();
  app.enableCors();

  await app.listen(process.env.PORT || 3000);
  console.log('========================> APP inited with env', process.env.NODE_ENV);
};

runApp().catch(console.error);
