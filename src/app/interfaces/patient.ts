import { Observable } from 'rxjs';
import { RSP } from 'app/interfaces';

export interface PatientGrpcService {
  createPatient(user: Patient): Observable<RSP<Patient>>;
  getPatient(param: PatientSearchparams): Observable<RSP<Patient>>;
  patientsList({ cabinetId }:{ cabinetId:string }): Observable<RSP<Patient[]>>;
  updatePatient(updateData: UpdatePatientParams): Observable<RSP<Patient>>;
}

export interface Patient {
  name?: string;
  surname?: string;
  secondname?: string;
  email: string;
  dob?: string;
  address?: string,
  phone?: string,
  createdAt?: Date,
  _id?: string,
  card_id?: string,
  periodentalChartId?: string,
  toothFormulaId?: string,
  cabinetId?: string,
  image?: string,
}

export interface PatientSearchparams {
  _id?: string,
  email?: string,
  cabinetId: string,
}

export interface UpdatePatientParams {
  cabinetId:string,
  patientId:string,
  patientData: Patient
}
