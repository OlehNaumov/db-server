import { Observable } from 'rxjs';
import { RSP } from 'app/interfaces';

export interface StatisticGrpcService {
  getGeneralStats(data: GeneralStatisticRequest, metadata?: any): Observable<RSP<GeneralStatistic>>;
}

export interface GeneralStatistic {
  doctorsCount: number;
  patientsCount: number;
}

export interface GeneralStatisticRequest {
  cabinetId: string;
}
