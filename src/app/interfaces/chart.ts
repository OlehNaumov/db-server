import { Observable } from 'rxjs';
import { RSP } from 'app/interfaces';

interface FiveZoneTooth {
  zone: { z1: boolean, z2: boolean, z3: boolean, z4: boolean, z5: boolean },
  removed: boolean,
  implant: boolean,
  crown: boolean,
  description: string,
  colors: { c1: string, c2: string, c3: string, c4: string }
}

interface FourZoneTooth {
  zone: { z1: boolean, z2: boolean, z3: boolean, z4: boolean },
  removed: boolean,
  implant: boolean,
  crown: boolean,
  description: string,
  colors: { c1: string, c2: string }
}

export interface ChartData {
  t18: FiveZoneTooth;
  t17: FiveZoneTooth;
  t16: FiveZoneTooth;
  t28: FiveZoneTooth;
  t27: FiveZoneTooth;
  t26: FiveZoneTooth;
  t38: FiveZoneTooth;
  t37: FiveZoneTooth;
  t36: FiveZoneTooth;
  t48: FiveZoneTooth;
  t47: FiveZoneTooth;
  t46: FiveZoneTooth;
  t15: FourZoneTooth;
  t14: FourZoneTooth;
  t13: FourZoneTooth;
  t12: FourZoneTooth;
  t11: FourZoneTooth;
  t25: FourZoneTooth;
  t24: FourZoneTooth;
  t23: FourZoneTooth;
  t22: FourZoneTooth;
  t21: FourZoneTooth;
  t35: FourZoneTooth;
  t34: FourZoneTooth;
  t33: FourZoneTooth;
  t32: FourZoneTooth;
  t31: FourZoneTooth;
  t45: FourZoneTooth;
  t44: FourZoneTooth;
  t43: FourZoneTooth;
  t42: FourZoneTooth;
  t41: FourZoneTooth;
}

export interface Chart {
  patientId: string
  createdAt?: number,
  notes?: string,
  chart: ChartData | any,
  _id?: string
}

export interface ChartGrpcService {
  getChart(request: GetChartRequest): Observable<RSP<Chart>>;
  updateChart(request: UpdateChartChartRequest): Observable<RSP<Chart>>;
}

export interface GetChartRequest{
  chartId: string,
  cabinetId: string
}
export interface UpdateChartChartRequest{
  chartId: string,
  cabinetId: string
  chart: ChartData
}
