import { Observable } from 'rxjs';
import { RSP } from 'app/interfaces';

export interface ScheduleGrpcService {
  getSchedule(searchSchedule: ScheduleSearchParams, metadata?: any): Observable<RSP<ScheduleItem[]>>;
}

export interface ScheduleSearchParams {
  tsFrom: number;
  tsTo: number;
  doctorsList?: string[]
}

export interface ScheduleItem {
  doctorName: string
  doctorSurname: string
  doctorSecondName: string
  doctorId: string
  patientName: string
  patientSurname: string
  patientSecondName: string
  patientId: string
  diseaseDate: number
  id?:string
  _id?:string
}
