import { Observable } from 'rxjs';
import { RSP } from 'app/interfaces';

export interface DiseaseGrpcService {
  addDisease(DiseaseRequest: Disease): Observable<RSP<Disease>>;
  getDiseaseList(diseaseId: DiseaseSearchParams,): Observable<RSP<Disease[]>>;
  updateDisease(diseaseId: Disease, metadata?: any): Observable<RSP<Disease>>;
}

export interface Disease {
  createdAt?: number;
  dateFrom?: number;
  dateTo?: number;
  name?: string;
  diagnosis?: string;
  price?: number;
  payed?: number;
  materials?: string;
  description?: string;
  doctorId: string;
  id?: string;
  cardId: string;
  cabinetId: string;
}

export interface DiseaseSearchParams {
  cardId: string;
  cabinetId?: string;
}
export interface DiseaseRequest {
  cardId: string;
  cabinetId?: string;
  disease: Disease;
}
