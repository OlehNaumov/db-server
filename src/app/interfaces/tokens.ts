import { BrowserType } from 'app/interfaces/general.interfaces';

export interface AccessToken{
  userId: string,
  token: string,
  refreshtoken: string,
  ip: string,
  createdAt?: Date
}

export interface TokenRequest{
  browser?:BrowserType,
  token?:string,
  refreshtoken?:string,
  ip?:string
}
