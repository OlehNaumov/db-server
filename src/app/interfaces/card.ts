import { Disease, RSP } from 'app/interfaces';
import { Observable } from 'rxjs';

export interface CardGrpcService {
  createCard(user: Card): Observable<RSP<Card>>;
  getCard(param: CardSearchParam): Observable<RSP<Card>>;
}
export interface Card {
  patientId?: string;
  description?: string;
  diseases?: Disease[];
  _id?: string;
}

export type CardSearchParam = {
  patientId?: string;
  cabinetId?: string;
  _id?: string
};
