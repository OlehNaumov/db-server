import { Observable } from 'rxjs';
import { RSP } from 'app/interfaces';

export interface PeriodentalTooth {
  absent: boolean,
  mobility: number,
  implant: boolean,
  furcation: number,
  bleeding: {
    v1: boolean,
    v2: boolean,
    v3: boolean,
  },
  pluc: number,
  margin: {
    v1: number,
    v2: number,
    v3: number,
  },
  depth: {
    v1: number,
    v2: number,
    v3: number,
  },
}

export interface UpPeriodental {
  t18: PeriodentalTooth,
  t17: PeriodentalTooth,
  t16: PeriodentalTooth,
  t15: PeriodentalTooth,
  t14: PeriodentalTooth,
  t13: PeriodentalTooth,
  t12: PeriodentalTooth,
  t11: PeriodentalTooth,
  t28: PeriodentalTooth,
  t27: PeriodentalTooth,
  t26: PeriodentalTooth,
  t25: PeriodentalTooth,
  t24: PeriodentalTooth,
  t23: PeriodentalTooth,
  t22: PeriodentalTooth,
  t21: PeriodentalTooth,
}
export interface BottomPeriodental {
  t48: PeriodentalTooth,
  t47: PeriodentalTooth,
  t46: PeriodentalTooth,
  t45: PeriodentalTooth,
  t44: PeriodentalTooth,
  t43: PeriodentalTooth,
  t42: PeriodentalTooth,
  t41: PeriodentalTooth,
  t38: PeriodentalTooth,
  t37: PeriodentalTooth,
  t36: PeriodentalTooth,
  t35: PeriodentalTooth,
  t34: PeriodentalTooth,
  t33: PeriodentalTooth,
  t32: PeriodentalTooth,
  t31: PeriodentalTooth,
}

export interface PeriodentalChart {
  up: {
    buccal: UpPeriodental,
    lingual: UpPeriodental,
  },
  down: {
    buccal: BottomPeriodental,
    lingual: BottomPeriodental,
  },
}
export interface PeriodentalChartData {
  patientId:string,
  chart: PeriodentalChart,
  id?: string
}

export interface PeriodentalGrpcService {
  getPeriodentalChart(params: { patientId: string, cabinetId: string }): Observable<RSP<PeriodentalChartData>>;
}
