import { Observable } from 'rxjs';
import { RSP } from 'app/interfaces';
import { TokenSet } from 'app/interfaces/auth';

export interface UserGrpcService {
  createUser(user: User, metadata?: any): Observable<RSP<User & TokenSet >>;
  getUser(param: UserSearchParam): Observable<RSP<User>>;
  acceptInvitation(user: User, metadata?: any): Observable<RSP<User>>;
  getUsersList(search:{ cabinetId:string }, metadata?: any): Observable<RSP<User[]>>;
  removeUser(param:{ _id:string }, metadata?: any): Observable<RSP<string>>;
}

export interface User {
  name?: string;
  surname?: string;
  secondname?: string;
  email: string;
  password?: string;
  hashedPassword?: string,
  roles?: string[],
  createdAt?: Date,
  _id?: string,
  id?: string,
  cabinetId?: string;
  cabinetsCount?: number;
  dob?:number;
  phone?:string;
  comparePasswords?(password: string): boolean;
  addCabinet?(): Promise<User>;
  removeCabinet?(): void;
}

export interface UserId {
  id: string;
}

export interface UserSearchParam {
  _id?: string;
  email?: string;
}

export interface Token {
  data?: string;
}
