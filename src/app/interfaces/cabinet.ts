import { Observable } from 'rxjs';
import { RSP } from 'app/interfaces/general.interfaces';

export interface CabinetGrpcService {
  createCabinet(cabinet: Cabinet, metadata?: any): Observable<RSP<Cabinet>>;
  getCabinet(userId: GetCabinetRequest): Observable<RSP<Cabinet>>;
  getInvitation(_id: Id): Observable<RSP<string>>;
  updateCabinet(cabinetData: Cabinet): Observable<RSP<Cabinet>>;
}

export interface Cabinet {
  name?: string;
  address?: string;
  logo?: string;
  phone?: string;
  additionalInfo?: string;
  createdAt?: number,
  users?: string[],
  usedInvitations?: string[],
  workHoursTo?: string,
  workHoursFrom?: string,
  _id?: string;
  id?: string;
  status?: string;
  inviteUser?(userId: string, token: string): any
}

export type Id = {
  _id: string
};
export type GetCabinetRequest = {
  userId: string
};
