export type BrowserType = {
  userAgent:string;
  ip:string
};

export type AppRequest = Request & {
  ip:string,
  headers:any,
  user: {
    cabinetId: string;
    email:string;
    roles: string[];
    id: string;
    iat: number,
    exp: number,
    browser: BrowserType
  }
};

export type RSP <T> = { data: T };
