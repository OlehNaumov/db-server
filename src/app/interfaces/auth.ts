import { Observable } from 'rxjs';
import { RSP } from 'app/interfaces';

export interface AuthGrpcService {
  login(loginData: LoginData, metadata?: any): Observable<RSP<TokenSet>>;
  logout(param:{ userId:string }, metadata?: any): Observable<RSP<string>>;
  refresh(param: TokenSet, metadata?: any): Observable<RSP<TokenSet>>;
}

export interface LoginData {
  email: string;
  password: string;
}

export interface TokenSet {
  token?: string;
  refreshtoken?: string;
}
