/* eslint-disable class-methods-use-this */
import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { InterceptorError } from 'app/error';

@Injectable()
export class HttpGlobalInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    // const data = context.switchToHttp().getResponse();

    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    return next.handle().pipe(map(({ error, data }) => {
      if (error) {
        throw new InterceptorError(error);
      }
      // eslint-disable-next-line @typescript-eslint/no-unsafe-return
      return data ?? {};
    }));
  }
}
