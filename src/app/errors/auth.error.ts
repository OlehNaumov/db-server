import { RpcException } from '@nestjs/microservices';
import { ErrorParams } from 'app/errors/error.interface';

class AuthError extends RpcException {
  public name:string;
  public code: number;
  public status: number;

  constructor({ code, message, status = 401 }: ErrorParams) {
    super({ message: JSON.stringify({ message, code, status, name: 'AUTHORIZATION ERROR' }), code: 300 });
    Error.captureStackTrace(this);

    // eslint-disable-next-line no-console
    console.log(this);
  }
}

export { AuthError };
