class BaseError extends Error {
  public name:string;
  public message:string;
  public status : number;

  constructor(error: { message:string, status:number }) {
    super(error.message);
    this.message = error.message;
    this.status = error.status || 500;
    this.name = 'BASE ERROR';
    Error.captureStackTrace(this);
  }
}

export { BaseError };
