/* eslint-disable class-methods-use-this */
import {
  Catch,
  RpcExceptionFilter,
  ArgumentsHost,
} from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';
import { Request, Response } from 'express';

interface ErrorBody {
  details: string;
  status?: number;
  message?: string;
  response: any;
  name?: string;
  statusCode?: number
}

const getErrorFromJSON = (jsonString: any): any => {
  try {
    return JSON.parse(jsonString) as ErrorBody;
  } catch (e) {
    return {
      message: jsonString,
      status: 500,
      name: 'SERVER ERROR',
    };
  }
};

@Catch()
export class ExceptionFiltering implements RpcExceptionFilter<RpcException> {
  catch(error: RpcException & ErrorBody, host: ArgumentsHost): any {
    const response: Response = host.switchToHttp().getResponse();
    const request: Request = host.switchToHttp().getRequest();

    const errorData: ErrorBody = error.details ? getErrorFromJSON(error.details)
      : error.response ?? { message: error.message, status: error.status, name: 'SERVER ERROR' };

    response.status(errorData.statusCode ?? errorData.status ?? 500);

    return response.send({
      timestamp: new Date().toISOString(),
      path: request.url,
      message: errorData.message,
      name: errorData.name,
    });
  }
}
