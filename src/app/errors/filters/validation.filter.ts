/* eslint-disable class-methods-use-this */
import { PipeTransform, ArgumentMetadata, HttpStatus } from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';
import { BaseError } from 'app/error';

export class ValidationFilter implements PipeTransform<any> {
  async transform(value: any, metadata: ArgumentMetadata): Promise<any> {
    // if (!value) {
    //   throw new BaseError({ message: 'No data submitted', status: 400 });
    // }
    const { metatype } = metadata;
    if (!metatype || this.isValidMetatype(metatype)) {
      return value;
    }
    const object = plainToClass(metatype, value);
    const errors = await validate(object);

    if (errors.length > 0) {
      throw new BaseError({
        message: `Input data validation failed ${this.buildError(errors)}`,
        status: HttpStatus.BAD_REQUEST,
      });
    }

    return value;
  }

  private buildError(errors: any[]) {
    return errors.map(({ constraints }) => `error: ${Object.values(constraints)[0]}`);
  }

  private isValidMetatype(metatype): boolean {
    const types = [String, Boolean, Number, Array, Object];
    return !!types.find((type) => metatype === type);
  }
}
