import { RpcException } from '@nestjs/microservices';
import { ErrorParams } from 'app/errors/error.interface';

class UserError extends RpcException {
  public name:string;
  public code: number;
  public status: number;

  constructor({ code, message, status = 500 }: ErrorParams) {
    super({ message: JSON.stringify({ message, code, status, name: 'USER ERROR' }), code: 300 });
    Error.captureStackTrace(this);

    // eslint-disable-next-line no-console
    console.log(this);
  }
}

export { UserError };
