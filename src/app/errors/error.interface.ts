interface ErrorInterface {
  message?:string,
  error?: { message:string }
}

export interface ErrorParams {
  error: ErrorInterface | string;
  status?: number;
  code?: number;
  message?: string
}

export interface BaseError {
  message: string;
  status: number;
  code: number;
  name: string;
}
