import { InterceptorError } from 'app/errors/interceptor.error';
import { AuthError } from 'app/errors/auth.error';
import { PatientError } from 'app/errors/patient.error';
import { CabinetError } from 'app/errors/cabinet.error';
import { UserError } from 'app/errors/user.error';
import { BaseError } from 'app/errors/BaseError';
import { DiseaseError } from 'app/errors/disease.error';
import { ChartError } from 'app/errors/chart.error';
import { CardError } from 'app/errors/card.error';
import { ScheduleError } from 'app/errors/schedule.error';
import { StatisticError } from 'app/errors/statistic.error';

export {
  InterceptorError,
  AuthError,
  PatientError,
  CabinetError,
  UserError,
  BaseError,
  DiseaseError,
  ChartError,
  CardError,
  ScheduleError,
  StatisticError,
};
