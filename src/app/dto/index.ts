export * from 'app/dto/user';

export * from 'app/dto/patient';
export * from 'app/dto/disease';
export * from 'app/dto/cabinet';
