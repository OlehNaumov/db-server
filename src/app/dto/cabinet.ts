import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class CabinetDto {
  @IsString()
  address:string;

  @IsNotEmpty() @IsString()
  name:string;

  phone:string;

  @IsEmail()
  adminEmail:string;

  date: number;
}
