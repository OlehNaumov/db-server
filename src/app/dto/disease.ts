/* eslint-disable max-classes-per-file */
import { IsNotEmpty } from 'class-validator';

export class DiseaseDto {
  @IsNotEmpty()
  dateFrom: number;

  @IsNotEmpty()
  dateTo: number;

  @IsNotEmpty()
  doctorId: string;
}
