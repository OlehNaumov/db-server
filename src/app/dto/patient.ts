import { IsEmail, IsNotEmpty } from 'class-validator';

export class PatientDto {
  @IsEmail()
  email: string;

  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  surname: string;

  @IsNotEmpty()
  secondname: string;
}