import { Injectable } from '@nestjs/common';
import { BaseError, ChartError } from 'app/error';
import { RSP, Chart, ChartData } from 'app/interfaces';
import { ChartDb } from 'db/db.queries';
import { deepMerge } from 'app/server/helpers';

@Injectable()
export class ChartService {
  constructor(private chartQuery: ChartDb) { }

  public async getChart(chartId:string, patientIds: string[]): Promise<RSP<Chart>> {
    try {
      const data = await this.chartQuery.getChart(chartId, patientIds);
      if (!data) { throw new BaseError({ message: 'Card not found', status: 404 }); }
      return { data };
    } catch (error) {
      throw new ChartError(error);
    }
  }

  public async updateChart(patientIds: string[], chartId:string, chart:ChartData): Promise<RSP<Chart>> {
    try {
      const chartModel = await this.chartQuery.getChart(chartId, patientIds);
      if (!chartModel) { throw new BaseError({ message: 'Card not found', status: 404 }); }
      const data = await chartModel.set({ chart: deepMerge(chartModel.toJSON().chart, chart) }).save();

      return { data };
    } catch (error) {
      throw new ChartError(error);
    }
  }
}
