import { Controller } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
import { GetChartRequest, RSP, Chart, UpdateChartChartRequest } from 'app/interfaces';
import { ChartService } from 'app/server/chart.module/chart.service';
import { CardService } from 'app/server/card.module/card.service';

@Controller()
export class ChartController {
  constructor(private chartService: ChartService, private cardService:CardService) { }

  @GrpcMethod('ChartController', 'getChart')
  async getChart({ cabinetId, chartId }: GetChartRequest): Promise<RSP<Chart>> {
    const patientsIds = await this.cardService.getCardIdsByCabinetId(cabinetId);
    return this.chartService.getChart(chartId, patientsIds);
  }

  @GrpcMethod('ChartController', 'updateChart')
  async updateChart({ cabinetId, chartId, chart }: UpdateChartChartRequest): Promise<RSP<Chart>> {
    const patientsIds = await this.cardService.getCardIdsByCabinetId(cabinetId);
    return this.chartService.updateChart(patientsIds, chartId, chart);
  }
}
