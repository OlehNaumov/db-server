import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ChartController } from 'app/server/chart.module/chart.controller';
import { ChartService } from 'app/server/chart.module/chart.service';
import { ChartDb } from 'db/db.queries';
import { ChartSchema } from 'db/schemas';
import { CardModule } from 'app/server/card.module/card.module';

@Module({
  exports: [ChartService],
  imports: [
    CardModule,
    // MongooseModule.forFeature([{ name: 'Card', schema: CardSchema }]),
    MongooseModule.forFeature([{ name: 'Chart', schema: ChartSchema }]),
  ],
  controllers: [ChartController],
  providers: [ChartDb, ChartService],
})

export class ChartModule { }
