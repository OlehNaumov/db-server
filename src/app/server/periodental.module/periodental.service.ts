import { Injectable } from '@nestjs/common';
import { PatientDb, PeriodentalChartDb } from 'db/db.queries';
import { PeriodentalChartData, RSP, PeriodentalChart } from 'app/interfaces';
import { BaseError, PatientError } from 'app/error';

@Injectable()
export class PeriodentalService {
  constructor(
    private periodentalChartQuery: PeriodentalChartDb,
    private patientQuery: PatientDb,
  ) { }

  public async getPeriodentalChart(cabinetId: string, patientId: string):Promise<RSP<PeriodentalChartData>> {
    try {
      const patient = await this.patientQuery.getPatient({ _id: patientId });
      if (!patient || patient.cabinetId.toString() !== cabinetId) {
        throw new BaseError({ message: 'Patient not found', status: 404 });
      }
      const data = await this.periodentalChartQuery.getPeriodentalChart(patientId);
      return { data };
    } catch (error) {
      throw new PatientError(error);
    }
  }
  public async createPeriodentalChart(patientId: string):Promise<{ patientId:string, chart:PeriodentalChart }> {
    return this.periodentalChartQuery.createPeriodentalChart(patientId);
  }
}
