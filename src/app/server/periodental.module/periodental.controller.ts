import { Controller } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
import { PeriodentalService } from 'app/server/periodental.module/periodental.service';
import { PeriodentalChartData, RSP } from 'app/interfaces';

@Controller()
export class PeriodentalController {
  constructor(private periodentalService: PeriodentalService) { }

  @GrpcMethod('PeriodentalController', 'getPeriodentalChart')
  async getPeriodentalChart(
    { cabinetId, patientId }:{ cabinetId:string, patientId:string },
  ): Promise<RSP<PeriodentalChartData>> {
    return this.periodentalService.getPeriodentalChart(cabinetId, patientId);
  }
}
