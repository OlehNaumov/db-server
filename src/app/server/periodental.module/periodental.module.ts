import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PeriodentalController } from 'app/server/periodental.module/periodental.controller';
import { PeriodentalService } from 'app/server/periodental.module/periodental.service';
import { CardDb, ChartDb, PatientDb, PeriodentalChartDb } from 'db/db.queries';
import { CardSchema, ChartSchema, PatientSchema, PeriodentalChartSchema } from 'db/schemas';

@Module({
  exports: [PeriodentalService],
  imports: [
    MongooseModule.forFeature([
      { name: 'Patient', schema: PatientSchema },
      { name: 'Card', schema: CardSchema },
      { name: 'Chart', schema: ChartSchema },
      { name: 'Periodental', schema: PeriodentalChartSchema },
    ]),
  ],
  controllers: [PeriodentalController],
  providers: [CardDb, ChartDb, PatientDb, PeriodentalChartDb, PeriodentalService],
})

export class PeriodentalModule { }
