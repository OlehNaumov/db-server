import { config } from 'node-config-ts';
import { JwtModule } from '@nestjs/jwt';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CabinetController } from 'app/server/cabinet.module/cabinet.controller';
import { CabinetService } from 'app/server/cabinet.module/cabinet.service';
import { CabinetDb, UserDb } from 'db/db.queries';
import { CabinetSchema, UserSchema } from 'db/schemas';

const { JWT } = config;

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Cabinet', schema: CabinetSchema },
      { name: 'User', schema: UserSchema },
    ]),

    JwtModule.register({
      secret: JWT.secret,
      verifyOptions: { algorithms: JWT.algorithms as any },
      signOptions: { algorithm: JWT.algorithm as any },
    }),
  ],
  controllers: [CabinetController],
  providers: [CabinetDb, UserDb, CabinetService],
})

export class CabinetModule { }
