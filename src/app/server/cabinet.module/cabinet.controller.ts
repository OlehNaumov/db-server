import { Controller } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
import { Metadata } from '@grpc/grpc-js';
import { CabinetService } from 'app/server/cabinet.module/cabinet.service';
import { Cabinet, RSP, Id, GetCabinetRequest } from 'app/interfaces';

@Controller()
export class CabinetController {
  constructor(private cabinetService: CabinetService) { }

  @GrpcMethod('CabinetController', 'getCabinet')
  async getCabinet({ userId }: GetCabinetRequest): Promise<RSP<Cabinet>> {
    return this.cabinetService.getCabinet(userId);
  }

  @GrpcMethod('CabinetController', 'createCabinet')
  async createCabinet(cabinetData: Cabinet, metadata: Metadata): Promise<RSP<Cabinet>> {
    const user = metadata.get('user')[0] as string;

    return this.cabinetService.createCabinet({
      ...cabinetData,
      users: [user],
    });
  }
  @GrpcMethod('CabinetController', 'updateCabinet')
  async updateCabinet(cabinetData: Cabinet): Promise<RSP<Cabinet>> {
    return this.cabinetService.updateCabinet(cabinetData);
  }

  @GrpcMethod('CabinetController', 'getInvitation')
  async getInvitation(userId: Id): Promise<RSP<string>> {
    return this.cabinetService.getInvitation(userId._id);
  }
}
