import { Injectable } from '@nestjs/common';
import { isValidObjectId } from 'mongoose';
import { CabinetDb, UserDb } from 'db/db.queries';
import { Cabinet, RSP } from 'app/interfaces';
import { CabinetError, BaseError } from 'app/error';
import { encrypt } from 'app/server/helpers';

@Injectable()
export class CabinetService {
  constructor(private cabinetQuery: CabinetDb, private userQuery: UserDb) { }

  public async getCabinet(userId: string): Promise<RSP<Cabinet>> {
    try {
      if (!isValidObjectId(userId)) {
        throw new BaseError({ message: 'Invalid id', status: 400 });
      }
      const data = await this.cabinetQuery.getCabinet(userId);
      if (!data) { throw new BaseError({ message: 'Cabinet not  found', status: 404 }); }

      return { data };
    } catch (error) {
      throw new CabinetError(error);
    }
  }

  public async createCabinet(cabinetData: Cabinet): Promise<RSP<Cabinet>> {
    try {
      const user = await this.userQuery.getUser({ _id: cabinetData.users[0] });
      if (user.cabinetsCount >= 1) {
        throw new CabinetError({ status: 403, error: 'This user already have cabinet' });
      }
      const data = await this.cabinetQuery.createCabinet(cabinetData);
      user.cabinetId = data._id;
      await user.addCabinet();
      return { data };
    } catch (error) {
      throw new CabinetError(error);
    }
  }

  public async getInvitation(userId: string): Promise<RSP<string>> {
    try {
      const { _id } = await this.cabinetQuery.getCabinet(userId);
      const data = encrypt(_id.toString());
      return { data };
    } catch (error) {
      throw new CabinetError(error);
    }
  }
  public async updateCabinet(cabinetData: Cabinet): Promise<RSP<Cabinet>> {
    try {
      const { id, ...cabinetRemainData } = cabinetData;
      const data = await this.cabinetQuery.updateCabinetById(cabinetData.id, cabinetRemainData);
      return { data };
    } catch (error) {
      throw new CabinetError(error);
    }
  }
}
