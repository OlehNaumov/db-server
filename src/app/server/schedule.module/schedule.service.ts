import { Injectable } from '@nestjs/common';
import { ScheduleSearchParams, RSP, ScheduleItem } from 'app/interfaces';
import { DiseaseDb } from 'db/db.queries';
import * as moment from 'moment';
import { ScheduleError } from 'app/error';

@Injectable()
export class ScheduleService {
  constructor(
    private diseaseQuery: DiseaseDb,

  ) { }

  public async getSchedule(
    cabinetId:string,
    { tsFrom, tsTo, doctorsList }: ScheduleSearchParams,
  ): Promise<RSP<ScheduleItem[]>> {
    try {
      const data = await this.diseaseQuery.getDiseasesWitFilter(cabinetId, {
        doctorsList,
        tsFrom: tsFrom ?? moment().startOf('week').unix(),
        tsTo: tsTo ?? moment().endOf('week').unix(),
      });

      return { data };
    } catch (error) {
      throw new ScheduleError(error);
    }
  }
}
