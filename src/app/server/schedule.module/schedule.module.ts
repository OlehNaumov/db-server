import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleController } from 'app/server/schedule.module/schedule.controller';
import { ScheduleService } from 'app/server/schedule.module/schedule.service';
import { DiseaseDb } from 'db/db.queries';
import { DiseaseSchema } from 'db/schemas';

@Module({
  exports: [ScheduleService],
  imports: [MongooseModule.forFeature([{ name: 'Disease', schema: DiseaseSchema }])],
  controllers: [ScheduleController],
  providers: [DiseaseDb, ScheduleService],
})
export class ScheduleModule { }
