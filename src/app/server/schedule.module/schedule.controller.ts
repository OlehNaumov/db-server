import { Controller } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
import { ScheduleSearchParams, ScheduleItem, RSP } from 'app/interfaces';
import { Metadata } from '@grpc/grpc-js';

import { ScheduleService } from 'app/server/schedule.module/schedule.service';

@Controller()
export class ScheduleController {
  constructor(private scheduleService: ScheduleService) { }

  @GrpcMethod('ScheduleController', 'getSchedule')
  async getSchedule(search: ScheduleSearchParams, metadata: Metadata): Promise<RSP<ScheduleItem[]>> {
    return this.scheduleService.getSchedule(metadata.get('cabinetId')[0] as string, search);
  }
}
