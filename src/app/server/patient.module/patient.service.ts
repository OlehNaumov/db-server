import { Injectable } from '@nestjs/common';
import { PatientError, BaseError } from 'app/error';
import {
  Patient,
  RSP,
  PatientSearchparams,
  UpdatePatientParams,
} from 'app/interfaces';
import { CardService } from 'app/server/card.module/card.service';
import { PeriodentalService } from 'app/server/periodental.module/periodental.service';
import { ChartDb, PatientDb } from 'db/db.queries';

@Injectable()
export class PatientService {
  constructor(
    private patientQuery: PatientDb,
    private chartDb: ChartDb,
    private readonly cardService: CardService,
    private readonly periodentalChartService: PeriodentalService,
  ) { }

  public async getPatient(param: PatientSearchparams): Promise<RSP<Patient>> {
    try {
      const data = await this.patientQuery.getPatient(param);
      if (!data) { throw new BaseError({ message: 'Patient not found', status: 404 }); }
      return { data };
    } catch (error) {
      throw new PatientError(error);
    }
  }

  public async createPatient(patientData: Patient): Promise<RSP<Patient>> {
    try {
      const existedPatient = await this.patientQuery.getPatient({
        cabinetId: patientData.cabinetId,
        name: patientData.name,
        surname: patientData.surname,
        secondname: patientData.secondname,
      });

      if (existedPatient) {
        throw new BaseError({ message: 'Patient already exists', status: 406 });
      }

      const patientModel = this.patientQuery.getNewPatientModel(patientData);
      const cardData = {
        patientId: patientModel._id,
        cabinetId: patientModel.cabinetId,
      };

      const [{ _id: cardId }, { _id: toothFormulaId }] = await Promise.all([
        this.cardService.createCard(cardData),
        this.chartDb.createChart({ patientId: patientModel._id, chart: {} }),
        this.periodentalChartService.createPeriodentalChart(patientModel._id),
      ]);

      const patient = await patientModel.set({ toothFormulaId, cardId }).save();

      return { data: patient };
    } catch (error) {
      throw new PatientError(error);
    }
  }

  public async patientsList(cabinetId: string): Promise<RSP<Patient[]>> {
    try {
      const data = await this.patientQuery.patientsList(cabinetId);
      return { data };
    } catch (error) {
      throw new PatientError(error);
    }
  }

  public async updatePatient({ cabinetId, patientData, patientId }: UpdatePatientParams): Promise<RSP<Patient>> {
    try {
      const patientModel = await this.patientQuery.getPatient({ _id: patientId, cabinetId });

      if (!patientModel) {
        throw new BaseError({ message: 'Patient not found', status: 404 });
      }

      const data = await this.patientQuery.updatePatientById(patientId, patientData);
      return { data };
    } catch (error) {
      throw new PatientError(error);
    }
  }
}
