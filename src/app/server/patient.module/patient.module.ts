import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CardModule } from 'app/server/card.module/card.module';
import { PeriodentalModule } from 'app/server/periodental.module/periodental.module';
import { PatientController } from 'app/server/patient.module/patient.controller';
import { PatientService } from 'app/server/patient.module/patient.service';
import { ChartDb, PatientDb } from 'db/db.queries';
import { CardSchema, ChartSchema, PatientSchema } from 'db/schemas';

@Module({
  imports: [
    CardModule,
    PeriodentalModule,
    MongooseModule.forFeature([
      { name: 'Patient', schema: PatientSchema },
      { name: 'Card', schema: CardSchema },
      { name: 'Chart', schema: ChartSchema },
    ]),
  ],
  controllers: [PatientController],
  providers: [PatientDb, ChartDb, PatientService],
})

export class PatientModule { }
