import { Controller } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
import { PatientService } from 'app/server/patient.module/patient.service';
import {
  Patient,
  RSP,
  PatientSearchparams,
  UpdatePatientParams,
} from 'app/interfaces';

@Controller()
export class PatientController {
  constructor(private patientService: PatientService) { }

  @GrpcMethod('PatientController', 'getPatient')
  async getPatient(param: PatientSearchparams): Promise<RSP<Patient>> {
    return this.patientService.getPatient(param);
  }

  @GrpcMethod('PatientController', 'createPatient')
  async createPatient(patientData: Patient): Promise<RSP<Patient>> {
    return this.patientService.createPatient(patientData);
  }

  @GrpcMethod('PatientController', 'patientsList')
  async patientsList(data: Patient): Promise<RSP<Patient[]>> {
    const response = await this.patientService.patientsList(data.cabinetId);
    return response;
  }

  @GrpcMethod('PatientController', 'updatePatient')
  async updatePatient(updatePatient: UpdatePatientParams):Promise<RSP<Patient>> {
    return this.patientService.updatePatient(updatePatient);
  }
}
