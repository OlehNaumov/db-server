import { Controller } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
import { RSP, Card, CardSearchParam } from 'app/interfaces';
import { CardService } from 'app/server/card.module/card.service';

@Controller()
export class CardController {
  constructor(private cardService: CardService) { }

  @GrpcMethod('CardController', 'getCard')
  async getCard(params: CardSearchParam): Promise<RSP<Card>> {
    return this.cardService.getCard(params);
  }
}
