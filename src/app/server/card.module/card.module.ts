import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CardController } from 'app/server/card.module/card.controller';
import { CardService } from 'app/server/card.module/card.service';
import { CardDb } from 'db/db.queries';
import { CardSchema, ChartSchema } from 'db/schemas';

@Module({
  exports: [CardService],
  imports: [
    MongooseModule.forFeature([
      { name: 'Card', schema: CardSchema },
      { name: 'Chart', schema: ChartSchema },
    ]),
  ],
  controllers: [CardController],
  providers: [CardDb, CardService],
})

export class CardModule { }
