import { Injectable } from '@nestjs/common';
import { CardError, BaseError } from 'app/error';
import { RSP, Card, CardSearchParam } from 'app/interfaces';
import { CardDb } from 'db/db.queries';

@Injectable()
export class CardService {
  constructor(private cardQuery: CardDb) { }

  public async getCard(params: CardSearchParam): Promise<RSP<Card>> {
    try {
      const data = await this.cardQuery.getCard(params);
      if (!data) { throw new BaseError({ message: 'Card not found', status: 404 }); }
      return { data };
    } catch (error) {
      throw new CardError(error);
    }
  }

  public async createCard(cardData: Card): Promise<Card> {
    return this.cardQuery.createCard(cardData);
  }

  public async getCardIdsByCabinetId(cabinetId: string):Promise<string[]> {
    const [cabinetPatients] = await this.cardQuery.getPatientsIdsInCabinet(cabinetId);
    return cabinetPatients?.patients ?? [];
  }
}
