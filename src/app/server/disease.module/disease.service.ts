import { Injectable } from '@nestjs/common';
import { Card, RSP, Disease, DiseaseSearchParams } from 'app/interfaces';
import { CardDb, DiseaseDb } from 'db/db.queries';
import { DiseaseError } from 'app/error';

@Injectable()
export class DiseaseService {
  constructor(
    private cardQuery: CardDb,
    private diseaseQuery: DiseaseDb,
  ) { }

  public async addDisease(disease: Disease): Promise<RSP<Card>> {
    try {
      await this.cardQuery.getCard({ _id: disease.cardId, cabinetId: disease.cabinetId });
      const data = await this.diseaseQuery.addDisease(disease);
      return { data };
    } catch (error) {
      throw new DiseaseError(error);
    }
  }

  public async getDiseaseList(search: DiseaseSearchParams): Promise<RSP<Disease[]>> {
    try {
      await this.cardQuery.getCard({ _id: search.cardId, cabinetId: search.cabinetId });
      const data = await this.diseaseQuery.getDiseaseList(search.cardId);

      return { data };
    } catch (error) {
      throw new DiseaseError(error);
    }
  }
  public async updateDisease(cabinetId: string, diseaseId:string, disease: Disease): Promise<RSP<Disease>> {
    try {
      const data = await this.diseaseQuery.updateDisease(cabinetId, diseaseId, disease);
      return { data };
    } catch (error) {
      throw new DiseaseError(error);
    }
  }
}
