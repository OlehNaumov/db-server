import { Controller } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
import {
  Disease,
  DiseaseSearchParams,
  RSP,
  Card,
} from 'app/interfaces';

import { DiseaseService } from 'app/server/disease.module/disease.service';
import { Metadata } from '@grpc/grpc-js';

@Controller()
export class DiseaseController {
  constructor(private diseaseService: DiseaseService) { }

  @GrpcMethod('DiseaseController', 'addDisease')
  async addDisease(disease: Disease): Promise<RSP<Card>> {
    return this.diseaseService.addDisease(disease);
  }

  @GrpcMethod('DiseaseController', 'getDiseaseList')
  async getDiseaseList(search: DiseaseSearchParams): Promise<RSP<Disease[]>> {
    return this.diseaseService.getDiseaseList(search);
  }

  @GrpcMethod('DiseaseController', 'updateDisease')
  async updateDisease(data: Disease, metadata: Metadata): Promise<RSP<Disease>> {
    const cabinetId = metadata.get('cabinetId')[0] as string;
    const diseaseId = metadata.get('diseaseId')[0] as string;
    return this.diseaseService.updateDisease(cabinetId, diseaseId, data);
  }
}
