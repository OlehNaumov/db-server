import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { DiseaseController } from 'app/server/disease.module/disease.controller';
import { DiseaseService } from 'app/server/disease.module/disease.service';
import { CardDb, DiseaseDb } from 'db/db.queries';
import { CardSchema, DiseaseSchema } from 'db/schemas';

@Module({
  exports: [DiseaseService],
  imports: [
    MongooseModule.forFeature([{ name: 'Card', schema: CardSchema }]),
    MongooseModule.forFeature([{ name: 'Disease', schema: DiseaseSchema }]),
  ],
  controllers: [DiseaseController],
  providers: [CardDb, DiseaseDb, DiseaseService],
})
export class DiseaseModule { }
