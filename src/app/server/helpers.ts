import { config } from 'node-config-ts';
import * as crypto from 'crypto';

// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
const { crypto: { invitation: CRYPTO_SECRET } } = config;

const ENCRYPTION_KEY = crypto.scryptSync(CRYPTO_SECRET, 'GfG', 32);
const IV_LENGTH = 16;

const encrypt = (text: string): string => {
  const iv = crypto.randomBytes(IV_LENGTH);
  const cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(ENCRYPTION_KEY), iv);
  const encrypted = Buffer.concat([cipher.update(text), cipher.final()]);

  return `${iv.toString('hex')}-${encrypted.toString('hex')}`;
};

const decrypt = (text: string): string => {
  const textParts = text.split('-');
  const iv = Buffer.from(textParts.shift(), 'hex');
  const encryptedText = Buffer.from(textParts.join(':'), 'hex');
  const decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(ENCRYPTION_KEY), iv);

  const decrypted = Buffer.concat([decipher.update(encryptedText), decipher.final()]);

  return decrypted.toString();
};

const deepMerge = (obj1: Record<string, any>, obj2:Record<string, any>): any => Object.entries(obj1).reduce(
  (accum, [key, value]) => (
    typeof value === 'object'
      ? { ...accum, [key]: deepMerge(value as Record<any, unknown>, obj2?.[key] as Record<any, unknown>) }
      : { ...accum, [key]: obj2?.[key] ?? value }
  ),
  {},
);

export {
  decrypt,
  encrypt,
  deepMerge,
};
