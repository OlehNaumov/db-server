import { Controller } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
import { StatisticService } from 'app/server/statistic.module/statistic.service';
import { GeneralStatisticRequest, GeneralStatistic, RSP } from 'app/interfaces';

@Controller()
export class StatisticController {
  constructor(private statsService: StatisticService) { }

  @GrpcMethod('StatisticController', 'getGeneralStats')
  async getGeneralStats({ cabinetId }: GeneralStatisticRequest):Promise<RSP<GeneralStatistic>> {
    return this.statsService.getGeneralStats(cabinetId);
  }
}
