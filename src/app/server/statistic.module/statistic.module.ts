import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { StatisticController } from 'app/server/statistic.module/statistic.controller';
import { StatisticService } from 'app/server/statistic.module/statistic.service';
import { UserSchema, PatientSchema } from 'db/schemas';
import { UserDb, PatientDb } from 'db/db.queries';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'User', schema: UserSchema },
      { name: 'Patient', schema: PatientSchema },
    ]),

  ],
  controllers: [StatisticController],
  providers: [StatisticService, UserDb, PatientDb],
})

export class StatisticModule { }
