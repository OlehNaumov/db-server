import { Injectable } from '@nestjs/common';
import { UserDb, PatientDb } from 'db/db.queries';
import { StatisticError } from 'app/error';
import { RSP, GeneralStatistic } from 'app/interfaces';

@Injectable()
export class StatisticService {
  constructor(
    private userQuery: UserDb,
    private patientQuery: PatientDb,
  ) {}

  public async getGeneralStats(cabinetId: string): Promise<RSP<GeneralStatistic>> {
    try {
      const [doctorsCount, patientsCount] = await Promise.all([
        this.userQuery.getUsersAmountByCabinetId(cabinetId),
        this.patientQuery.getPatientsAmountByCabinetId(cabinetId),
      ]);
      return { data: { doctorsCount, patientsCount } };
    } catch (error) {
      throw new StatisticError(error);
    }
  }
}
