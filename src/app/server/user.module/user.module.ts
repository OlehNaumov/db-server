import { config } from 'node-config-ts';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserController } from 'app/server/user.module/user.controller';
import { UserService } from 'app/server/user.module/user.service';
import { UserSchema, CabinetSchema, AccessTokenSchema } from 'db/schemas';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { UserDb, CabinetDb, TokenDb } from 'db/db.queries';

const { JWT } = config;

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'User', schema: UserSchema },
      { name: 'AccessToken', schema: AccessTokenSchema },
      { name: 'Cabinet', schema: CabinetSchema },
    ]),
    PassportModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: JWT.secret,
      verifyOptions: { algorithms: JWT.algorithms as any },
      signOptions: { algorithm: JWT.algorithm as any, expiresIn: JWT.expire },
    }),
  ],
  controllers: [UserController],
  providers: [UserService, UserDb, CabinetDb, TokenDb],
})

export class UserModule { }
