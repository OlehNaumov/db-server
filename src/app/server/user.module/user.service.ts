import { config } from 'node-config-ts';
import { Injectable } from '@nestjs/common';
import {
  User,
  UserSearchParam,
  BrowserType,
  TokenSet,
  RSP,
} from 'app/interfaces';
import { JwtService } from '@nestjs/jwt';
import { UserDb, CabinetDb } from 'db/db.queries';
import { BaseError, UserError } from 'app/error';
import { parseUserData } from 'app/server/user.module/helpers';
import { decrypt } from 'app/server/helpers';
import { USER_ROLES } from 'db/constants';

// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
const { UNLIMITED_JWT } = config;

@Injectable()
export class UserService {
  private UNLIMITED_JWT: JwtService;

  constructor(
    private readonly jwt: JwtService,
    private userQuery: UserDb,
    private cabinetQuery: CabinetDb,
  ) {
    this.UNLIMITED_JWT = new JwtService(UNLIMITED_JWT as any);
  }

  public async createUser(
    userData: User,
    browser: BrowserType,
  ): Promise<RSP<User & TokenSet>> {
    try {
      const userModel = this.userQuery.getUserModel({
        roles: Object.keys(USER_ROLES),
        cabinetsCount: 1,
        ...userData,
      });
      const cabinet = await this.cabinetQuery.createCabinet({ users: [userModel._id] });
      const token = await this.jwt.signAsync({ browser, ...parseUserData(userModel, cabinet._id) });
      const refreshtoken = await this.UNLIMITED_JWT.signAsync({ id: userModel._id, ip: browser.ip });
      await userModel.set({ cabinetId: cabinet._id }).save();
      const data = {
        token,
        refreshtoken,
        name: userModel.name,
        surname: userModel.surname,
        secondname: userModel.secondname,
        email: userModel.email,
        createdAt: userModel.createdAt,
        id: userModel._id,
        dob: userModel.dob,
        phone: userModel.phone,
      };
      return { data };
    } catch (error) {
      throw new UserError(error);
    }
  }

  public async getUser(param: UserSearchParam): Promise<RSP<User>> {
    try {
      const data = await this.userQuery.getUser(param);
      if (!data) { throw new BaseError({ message: 'User not found', status: 404 }); }
      return { data };
    } catch (error) {
      throw new UserError({ error });
    }
  }

  public async acceptInvitation(userData: User, token: string): Promise<RSP<User>> {
    try {
      const userModel = this.userQuery.getUserModel({ roles: [USER_ROLES.DOCTOR], ...userData });
      const cabinetId = decrypt(token);
      const cabinet = await this.cabinetQuery.getCabinetById(cabinetId);
      userModel.set({ cabinetId, cabinetsCount: 1 });
      await cabinet.inviteUser(userModel._id, token);
      const data = await userModel.save();
      return { data };
    } catch (error) {
      throw new UserError(error);
    }
  }

  public async getUsersList(cabinetId: string): Promise<RSP<User[]>> {
    try {
      return { data: await this.userQuery.getUsersList(cabinetId) };
    } catch (error) {
      throw new UserError(error);
    }
  }
  public async removeUser(removeId: string, user: User): Promise<RSP<string>> {
    try {
      const userModel = await this.userQuery.getUser({ _id: removeId });
      if (`${user.cabinetId}` === `${userModel?.cabinetId}`) {
        await this.userQuery.removeUser(removeId);
        return { data: removeId };
      }
      throw new BaseError({ message: 'Not permitted', status: 403 });
    } catch (error) {
      throw new UserError(error);
    }
  }
}
