import { Metadata } from '@grpc/grpc-js';
import { Controller } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
import { UserService } from 'app/server/user.module/user.service';
import {
  User,
  RSP,
  UserSearchParam,
} from 'app/interfaces';

@Controller()
export class UserController {
  constructor(private userService: UserService) { }

  @GrpcMethod('UserController', 'createUser')
  async createUser(user: User, metadata: Metadata): Promise<RSP<User>> {
    const browser = JSON.parse(metadata.get('browser')?.[0] as string);
    return this.userService.createUser(user, browser);
  }

  @GrpcMethod('UserController', 'getUser')
  async getUser(searchParam: UserSearchParam):Promise<RSP<User>> {
    return this.userService.getUser(searchParam);
  }

  @GrpcMethod('UserController', 'acceptInvitation')
  async acceptInvitation(user: User, metadata: Metadata): Promise<RSP<User>> {
    const token: string = metadata.get('token')[0] as string;
    return this.userService.acceptInvitation(user, token);
  }

  @GrpcMethod('UserController', 'getUsersList')
  async getUsersList({ cabinetId }: { cabinetId:string }): Promise<RSP<User[]>> {
    return this.userService.getUsersList(cabinetId);
  }

  @GrpcMethod('UserController', 'removeUser')
  async removeUser({ _id: removeId }: { _id:string }, metadata: Metadata): Promise<RSP<string>> {
    const user: User = JSON.parse(metadata.get('user')[0] as string) as User;
    return this.userService.removeUser(removeId, user);
  }
}
