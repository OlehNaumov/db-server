import { User } from 'app/interfaces';

export const parseUserData = (user: User, cabinetId?: string): User => ({
  cabinetId: user.cabinetId ?? cabinetId,
  email: user.email,
  roles: user.roles,
  id: user._id,
});
