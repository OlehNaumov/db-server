import { config } from 'node-config-ts';
import { Injectable } from '@nestjs/common';
import {
  User,
  BrowserType,
  TokenSet,
  RSP,
} from 'app/interfaces';
import { JwtService } from '@nestjs/jwt';
import { UserDb, CabinetDb, TokenDb } from 'db/db.queries';
import { BaseError, UserError } from 'app/error';
import { parseUserData } from 'app/server/user.module/helpers';

// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
const { UNLIMITED_JWT, JWT: { secret } } = config;

@Injectable()
export class AuthService {
  private UNLIMITED_JWT: JwtService;

  constructor(
    private readonly jwt: JwtService,
    private userQuery: UserDb,
    private cabinetQuery: CabinetDb,
    private tokenQuery: TokenDb,
  ) {
    this.UNLIMITED_JWT = new JwtService(UNLIMITED_JWT as any);
  }

  public async login(
    { email, password }: { email: string, password: string },
    browser: BrowserType,
  ): Promise<RSP<TokenSet>> {
    try {
      const user = await this.userQuery.getUser({ email });

      if (!user || !user.comparePasswords(password)) {
        throw new BaseError({ message: 'Unauthorized', status: 401 });
      }

      const { _id: cabinetId } = await this.cabinetQuery.getCabinet(user._id);

      const token = await this.jwt.signAsync({ browser, ...parseUserData(user, cabinetId) });

      const refreshtoken = await this.UNLIMITED_JWT.signAsync({ id: user._id, ip: browser.ip });
      return { data: { token, refreshtoken } };
    } catch (error) {
      throw new UserError(error);
    }
  }

  public async logout(
    userId: string,
    data: { ip: string, token: string, refreshtoken: string },
  ): Promise<RSP<string>> {
    try {
      const verifiedToken: User = await this.jwt.verifyAsync(data.token, { secret });
      const verifiedRefreshToken: User & { ip: string } = await this.jwt.verifyAsync(data.refreshtoken, { secret });
      if (userId !== verifiedToken.id || verifiedRefreshToken.id !== userId || data.ip !== verifiedRefreshToken.ip) {
        throw new BaseError({ message: 'Not permitted', status: 403 });
      }
      await this.tokenQuery.restrictToken({ userId, ...data });
      return { data: userId };
    } catch (error) {
      throw new UserError(error);
    }
  }

  public async refresh(oldToken: string, oldRefreshtoken: string, browser: BrowserType): Promise<RSP<TokenSet>> {
    try {
      const verifiedToken: User = await this.jwt.verifyAsync(oldToken, { secret });
      const verifiedRefreshToken: User & { ip: string } = await this.jwt.verifyAsync(oldRefreshtoken, { secret });

      if (verifiedRefreshToken.ip !== browser.ip || verifiedRefreshToken.id !== verifiedToken.id) {
        throw new BaseError({ message: 'Not permitted', status: 403 });
      }

      const userModel = await this.userQuery.getUser({ _id: verifiedRefreshToken.id });
      const token = await this.jwt.signAsync({ browser, ...parseUserData(userModel) });
      const refreshtoken = await this.UNLIMITED_JWT.signAsync({ id: userModel._id, ip: browser.ip });

      return {
        data: {
          token,
          refreshtoken,
        },
      };
    } catch (error) {
      throw new UserError(error);
    }
  }
}
