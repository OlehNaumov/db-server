import { Metadata } from '@grpc/grpc-js';
import { Controller } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
import { AuthService } from 'app/server/auth.module/auth.service';
import {
  RSP,
  LoginData,
  TokenSet,
  BrowserType,
} from 'app/interfaces';

@Controller()
export class AuthController {
  constructor(private authService: AuthService) { }

  @GrpcMethod('AuthController', 'login')
  async login(loginData: LoginData, metadata: Metadata): Promise<RSP<TokenSet>> {
    const browser = JSON.parse(metadata.get('browser')?.[0] as string);
    return this.authService.login(loginData, browser);
  }

  @GrpcMethod('AuthController', 'logout')
  async logout({ userId }: { userId:string }, metadata: Metadata): Promise<RSP<string>> {
    const browser: BrowserType = JSON.parse(metadata.get('browser')[0] as string) as BrowserType;
    const token: string = metadata.get('token')[0] as string;
    const refreshtoken: string = metadata.get('refreshtoken')[0] as string;

    return this.authService.logout(userId, { ip: browser.ip, token, refreshtoken });
  }

  @GrpcMethod('AuthController', 'refresh')
  async refresh({ token, refreshtoken }: TokenSet, metadata: Metadata): Promise<RSP<TokenSet>> {
    const browser: BrowserType = JSON.parse(metadata.get('browser')[0] as string) as BrowserType;
    return this.authService.refresh(token, refreshtoken, browser);
  }
}
