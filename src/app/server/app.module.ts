import { Module } from '@nestjs/common';
import { CabinetModule } from 'app/server/cabinet.module/cabinet.module';
import { CardModule } from 'app/server/card.module/card.module';
import { DiseaseModule } from 'app/server/disease.module/disease.module';
import { PatientModule } from 'app/server/patient.module/patient.module';
import { UserModule } from 'app/server/user.module/user.module';
import { ChartModule } from 'app/server/chart.module/chart.module';
import { ScheduleModule } from 'app/server/schedule.module/schedule.module';
import { PeriodentalModule } from 'app/server/periodental.module/periodental.module';
import { StatisticModule } from 'app/server/statistic.module/statistic.module';
import { AuthModule } from 'app/server/auth.module/auth.module';
import { DbModule } from 'db/db.module';

@Module({
  imports: [
    DbModule,
    UserModule,
    CabinetModule,
    PatientModule,
    CardModule,
    DiseaseModule,
    ChartModule,
    ScheduleModule,
    PeriodentalModule,
    StatisticModule,
    AuthModule,
  ],
  controllers: [],
  providers: [],
})
export class ServerAppModule { }
