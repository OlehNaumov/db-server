import { Module } from '@nestjs/common';
import { CabinetController } from 'app/client/cabinet.api/cabinet.controller';
import { CabinetService } from 'app/client/cabinet.api/cabinet.service';
import { AccessTokenSchema } from 'db/schemas';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'AccessToken', schema: AccessTokenSchema }]),
  ],
  controllers: [CabinetController],
  providers: [CabinetService],
})

export class CabinetModule { }
