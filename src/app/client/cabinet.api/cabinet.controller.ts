import {
  Controller,
  Put,
  Get,
  UseGuards,
  Request,
  Body,
  SetMetadata,
  UseInterceptors,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CabinetService } from 'app/client/cabinet.api/cabinet.service';
import { AppRequest, Cabinet, RSP } from 'app/interfaces';
import { Observable } from 'rxjs';
import { RolesGuard, IpGuard } from 'app/client/api.guards';
import { CabinetDto } from 'app/dto';
import { USER_ROLES } from 'db/constants';
import { DefaultResponse } from 'app/client/response.interceptor';

@Controller()
export class CabinetController {
  constructor(private cabinetService: CabinetService) { }

  // @Post('cabinet')
  // @UseGuards(AuthGuard('jwt'))
  // @SetMetadata('roles', ['admin'])
  // @UseGuards(AuthGuard('jwt'), RolesGuard)
  // createCabinet(@Body() cabinet: Cabinet, @Request() req: any) {
  //   return this.cabinetService.createCabinet(cabinet, req.user._id);
  // }

  @Get('cabinet')
  @UseGuards(AuthGuard('jwt'), IpGuard)
  @UseInterceptors(new DefaultResponse({}))
  getCabinet(@Request() req: AppRequest): Observable<RSP<Cabinet>> {
    return this.cabinetService.getCabinet(req.user.id);
  }

  @SetMetadata('roles', [USER_ROLES.ADMIN, USER_ROLES.ROOT])
  @UseGuards(AuthGuard('jwt'), RolesGuard, IpGuard)
  @Put('cabinet')
  @UseInterceptors(new DefaultResponse({}))
  updateCabinet(@Request() req: AppRequest, @Body() cabinet: CabinetDto): Observable<RSP<Cabinet>> {
    return this.cabinetService.updateCabinet(req.user.cabinetId, cabinet);
  }

  @Get('cabinet/invitation')
  @UseGuards(AuthGuard('jwt'), IpGuard)
  @UseInterceptors(new DefaultResponse(''))
  getInvitation(@Request() req: AppRequest): Observable<RSP<string>> {
    return this.cabinetService.getInvitation(req.user.id);
  }
}
