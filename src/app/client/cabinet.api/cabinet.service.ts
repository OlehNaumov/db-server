import { Injectable, OnModuleInit } from '@nestjs/common';
import {
  CabinetGrpcService,
  Cabinet,
  RSP,
} from 'app/interfaces';
import { Client, ClientGrpc } from '@nestjs/microservices';
import { Metadata } from '@grpc/grpc-js';
import { cabinetGrpcOptions } from 'app/options_grpc';

import { Observable } from 'rxjs';

@Injectable()
export class CabinetService implements OnModuleInit {
  @Client(cabinetGrpcOptions)
  private client: ClientGrpc;

  private cabinetGrpcService: CabinetGrpcService;

  onModuleInit(): void {
    this.cabinetGrpcService = this.client.getService<CabinetGrpcService>('CabinetController');
  }

  createCabinet(cabinet: Cabinet, user_id: string): Observable<RSP<Cabinet>> {
    const metadata = new Metadata();
    metadata.add('user', user_id);
    return this.cabinetGrpcService.createCabinet(cabinet, metadata);
  }

  getCabinet(userId: string): Observable<RSP<Cabinet>> {
    return this.cabinetGrpcService.getCabinet({ userId });
  }

  getInvitation(_id: string): Observable<RSP<string>> {
    return this.cabinetGrpcService.getInvitation({ _id });
  }

  updateCabinet(cabinetId: string, cabinetData: Cabinet): Observable<RSP<Cabinet>> {
    return this.cabinetGrpcService.updateCabinet({ id: cabinetId, ...cabinetData });
  }
}
