import { config } from 'node-config-ts';
import { JwtModule } from '@nestjs/jwt';
import { Module } from '@nestjs/common';
import { RouterModule, Routes } from 'nest-router';
import { PassportModule } from '@nestjs/passport';
import { RolesGuard, IpGuard } from 'app/client/api.guards';
import { UserModule } from 'app/client/user.api/user.module';
import { PatientModule } from 'app/client/patient.api/patient.module';
import { CabinetModule } from 'app/client/cabinet.api/cabinet.module';
import { CardModule } from 'app/client/card.api/card.module';
import { DiseaseModule } from 'app/client/disease.api/disease.module';
import { ChartModule } from 'app/client/chart.api/chart.module';
import { ScheduleModule } from 'app/client/schedule.api/schedule.module';
import { PeriodentalModule } from 'app/client/periodental.api/periodental.module';
import { StatisticModule } from 'app/client/statistics.api/statistics.module';
import { AuthModule } from 'app/client/auth.api/auth.module';
import { JwtStrategy } from 'app/client/jwt.strategy';
import { MongooseModule } from '@nestjs/mongoose';
import { CabinetSchema, AccessTokenSchema } from 'db/schemas';

const { JWT } = config;

const Routers: Routes = [
  {
    path: '/api',
    children: [
      {
        path: '/v1',
        children: [
          UserModule,
          PatientModule,
          CabinetModule,
          CardModule,
          DiseaseModule,
          ChartModule,
          ScheduleModule,
          PeriodentalModule,
          StatisticModule,
          AuthModule,
        ],
      },
    ],
  },
];

@Module({
  imports: [
    RouterModule.forRoutes(Routers),
    UserModule,
    PatientModule,
    CabinetModule,
    CardModule,
    DiseaseModule,
    ChartModule,
    ScheduleModule,
    PeriodentalModule,
    StatisticModule,
    AuthModule,

    MongooseModule.forFeature([
      { name: 'Cabinet', schema: CabinetSchema },
      { name: 'AccessToken', schema: AccessTokenSchema },
    ]),
    PassportModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: JWT.secret,
      verifyOptions: { algorithms: JWT.algorithms as any },
      signOptions: { algorithm: JWT.algorithm as any, expiresIn: JWT.expire },
    }),
  ],
  controllers: [],
  providers: [JwtStrategy, RolesGuard, IpGuard],
})
export class ClientAppModule { }
