import { Module } from '@nestjs/common';
import { ChartController } from 'app/client/chart.api/chart.controller';
import { ChartService } from 'app/client/chart.api/chart.service';
import { AccessTokenSchema } from 'db/schemas';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'AccessToken', schema: AccessTokenSchema }])],
  controllers: [ChartController],
  providers: [ChartService],
})

export class ChartModule { }
