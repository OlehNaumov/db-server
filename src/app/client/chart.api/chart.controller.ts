import {
  Controller,
  Req,
  Param,
  Get,
  Put,
  UseGuards,
  SetMetadata,
  Body,
  UseInterceptors,
} from '@nestjs/common';

import { AuthGuard } from '@nestjs/passport';
import { RolesGuard, IpGuard } from 'app/client/api.guards';
import { ChartService } from 'app/client/chart.api/chart.service';
import { AppRequest, RSP, Chart, ChartData } from 'app/interfaces';
import { Observable } from 'rxjs';
import { USER_ROLES } from 'db/constants';
import { DefaultResponse } from 'app/client/response.interceptor';

@Controller()
export class ChartController {
  constructor(private chartService: ChartService) { }

  @SetMetadata('roles', [USER_ROLES.ADMIN, USER_ROLES.ROOT])
  @UseGuards(AuthGuard('jwt'), RolesGuard, IpGuard)
  @Get('chart/:chartId')
  @UseInterceptors(new DefaultResponse({}))
  getChartData(@Req() req: AppRequest, @Param('chartId') chartId: string): Observable<RSP<Chart>> {
    return this.chartService.getChart({ chartId, cabinetId: req.user.cabinetId });
  }

  @UseGuards(AuthGuard('jwt'), RolesGuard, IpGuard)
  @Put('chart/:chartId')
  @UseInterceptors(new DefaultResponse({}))
  updateChartData(
    @Req() req: AppRequest, @Param('chartId') chartId:string, @Body() chartData: ChartData,
  ): Observable<RSP<Chart>> {
    return this.chartService.updateChart(req.user.cabinetId, chartId, chartData);
  }
}
