import { Injectable, OnModuleInit } from '@nestjs/common';
import { Client, ClientGrpc } from '@nestjs/microservices';
import { ChartGrpcService, Chart, RSP, GetChartRequest, ChartData } from 'app/interfaces';
import { chartGrpcOptions } from 'app/options_grpc';
import { Observable } from 'rxjs';

@Injectable()
export class ChartService implements OnModuleInit {
  @Client(chartGrpcOptions)
  private client: ClientGrpc;

  private chartGrpcService: ChartGrpcService;

  onModuleInit():void {
    this.chartGrpcService = this.client.getService<ChartGrpcService>('ChartController');
  }

  getChart(params :GetChartRequest): Observable<RSP<Chart>> {
    return this.chartGrpcService.getChart(params);
  }

  updateChart(cabinetId:string, chartId:string, chart :ChartData): Observable<RSP<Chart>> {
    return this.chartGrpcService.updateChart({ cabinetId, chart, chartId });
  }
}
