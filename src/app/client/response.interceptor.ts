import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class DefaultResponse implements NestInterceptor {
  constructor(private defaultValue: any[] | any) { }
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next
      .handle()
      .pipe(
        map((value) => ((!value || Object.keys(value).length === 0) ? { data: this.defaultValue } : value)),
      );
  }
}
