import { Injectable, OnModuleInit } from '@nestjs/common';
import { Client, ClientGrpc } from '@nestjs/microservices';
import { PeriodentalGrpcService, RSP, PeriodentalChartData } from 'app/interfaces';
import { periodentalGrpcOptions } from 'app/options_grpc';
import { Observable } from 'rxjs';

@Injectable()
export class PeriodentalService implements OnModuleInit {
  @Client(periodentalGrpcOptions)
  private client: ClientGrpc;

  private periodentalGrpcService: PeriodentalGrpcService;

  onModuleInit(): void {
    this.periodentalGrpcService = this.client.getService<PeriodentalGrpcService>('PeriodentalController');
  }

  getPeriodentalChart(cabinetId: string, patientId:string): Observable<RSP<PeriodentalChartData>> {
    return this.periodentalGrpcService.getPeriodentalChart({ cabinetId, patientId });
  }
}
