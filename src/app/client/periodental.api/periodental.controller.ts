import {
  Controller,
  Req,
  Param,
  Get,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { PeriodentalService } from 'app/client/periodental.api/periodental.service';
import { RolesGuard, IpGuard } from 'app/client/api.guards';
import { AppRequest, RSP, PeriodentalChartData } from 'app/interfaces';
import { Observable } from 'rxjs';
import { DefaultResponse } from 'app/client/response.interceptor';

@Controller()
export class PeriodentalController {
  constructor(private periodentalService: PeriodentalService) {}

  @UseGuards(AuthGuard('jwt'), RolesGuard, IpGuard)
  @Get('periodental/:patientId')
  @UseInterceptors(new DefaultResponse({}))
  getPeriodentalChart(
    @Req() req: AppRequest, @Param('patientId') patientId: string,
  ): Observable<RSP<PeriodentalChartData>> {
    const { cabinetId } = req.user;
    return this.periodentalService.getPeriodentalChart(cabinetId, patientId);
  }
}
