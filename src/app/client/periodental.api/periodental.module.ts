import { Module } from '@nestjs/common';
import { PeriodentalController } from 'app/client/periodental.api/periodental.controller';
import { PeriodentalService } from 'app/client/periodental.api/periodental.service';
import { AccessTokenSchema } from 'db/schemas';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'AccessToken', schema: AccessTokenSchema }])],
  controllers: [PeriodentalController],
  providers: [PeriodentalService],
  exports: [PeriodentalService],
})

export class PeriodentalModule { }
