import { Module } from '@nestjs/common';
import { CardController } from 'app/client/card.api/card.controller';
import { CardService } from 'app/client/card.api/card.service';
import { AccessTokenSchema } from 'db/schemas';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'AccessToken', schema: AccessTokenSchema }]),
  ],
  controllers: [CardController],
  providers: [CardService],
  exports: [CardService],
})

export class CardModule { }
