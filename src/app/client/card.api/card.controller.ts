import {
  Controller,
  Req,
  Param,
  Get,
  UseGuards,
  SetMetadata,
  UseInterceptors,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CardService } from 'app/client/card.api/card.service';
import { RolesGuard, IpGuard } from 'app/client/api.guards';
import { AppRequest, Card, RSP } from 'app/interfaces';
import { Observable } from 'rxjs';
import { USER_ROLES } from 'db/constants';
import { DefaultResponse } from 'app/client/response.interceptor';

@Controller()
export class CardController {
  constructor(private cardService: CardService) {}

  @SetMetadata('roles', [USER_ROLES.ADMIN, USER_ROLES.ROOT])
  @UseGuards(AuthGuard('jwt'), RolesGuard, IpGuard)
  @Get('card/:patientId')
  @UseInterceptors(new DefaultResponse({}))
  getPatientCard(@Req() req: AppRequest, @Param('patientId') patientId: string): Observable<RSP<Card>> {
    const { cabinetId } = req.user;
    return this.cardService.getCard({ cabinetId, patientId });
  }
}
