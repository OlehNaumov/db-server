import { Injectable, OnModuleInit } from '@nestjs/common';
import { Client, ClientGrpc } from '@nestjs/microservices';
import {
  CardGrpcService,
  RSP,
  Card,
  CardSearchParam,
} from 'app/interfaces';
import { cardGrpcOptions } from 'app/options_grpc';
import { Observable } from 'rxjs';

@Injectable()
export class CardService implements OnModuleInit {
  @Client(cardGrpcOptions)
  private client: ClientGrpc;

  private cardGrpcService: CardGrpcService;

  onModuleInit(): void {
    this.cardGrpcService = this.client.getService<CardGrpcService>(
      'CardController',
    );
  }

  getCard(params: CardSearchParam): Observable<RSP<Card>> {
    return this.cardGrpcService.getCard(params);
  }
}
