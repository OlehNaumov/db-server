import { Module } from '@nestjs/common';
import { PatientController } from 'app/client/patient.api/patient.controller';
import { PatientService } from 'app/client/patient.api/patient.service';
import { AccessTokenSchema } from 'db/schemas';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'AccessToken', schema: AccessTokenSchema }]),
  ],
  controllers: [PatientController],
  providers: [PatientService],
})

export class PatientModule { }
