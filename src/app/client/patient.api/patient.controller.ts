import {
  Controller,
  Req,
  Post,
  Put,
  Param,
  Get,
  UseGuards,
  SetMetadata,
  Body,
  UseInterceptors,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { PatientService } from 'app/client/patient.api/patient.service';
import { RolesGuard, IpGuard } from 'app/client/api.guards';
import {
  Patient,
  AppRequest,
  RSP,
} from 'app/interfaces';
import { PatientDto } from 'app/dto/';
import { Observable } from 'rxjs';
import { USER_ROLES } from 'db/constants';
import { DefaultResponse } from 'app/client/response.interceptor';

@Controller()
export class PatientController {
  constructor(private patientService: PatientService) { }

  @SetMetadata('roles', Object.values(USER_ROLES))
  @UseGuards(AuthGuard('jwt'), RolesGuard, IpGuard)
  @Get('patient/list')
  @UseInterceptors(new DefaultResponse([]))
  getPatientList(@Req() req: AppRequest): Observable<RSP<Patient[]>> {
    const cabinet = req.user.cabinetId;
    return this.patientService.patientList(cabinet);
  }

  @SetMetadata('roles', [USER_ROLES.ADMIN, USER_ROLES.ROOT])
  @UseGuards(AuthGuard('jwt'), RolesGuard, IpGuard)
  @Get('patient/:patientId')
  @UseInterceptors(new DefaultResponse({}))
  getPatient(@Req() req: AppRequest, @Param('patientId') _id: string): Observable<RSP<Patient>> {
    const { cabinetId } = req.user;
    return this.patientService.getPatient({ _id, cabinetId });
  }

  @SetMetadata('roles', [USER_ROLES.ADMIN, USER_ROLES.ROOT])
  @UseGuards(AuthGuard('jwt'), RolesGuard, IpGuard)
  @Put('patient/:patientId')
  @UseInterceptors(new DefaultResponse({}))
  updatePatient(
    @Req() req: AppRequest, @Param('patientId') patientId: string, @Body() patientData: PatientDto,
  ): Observable<RSP<Patient>> {
    const { cabinetId } = req.user;
    return this.patientService.updatePatient(cabinetId, patientId, patientData);
  }

  @SetMetadata('roles', Object.values(USER_ROLES))
  @UseGuards(AuthGuard('jwt'), RolesGuard, IpGuard)
  @Post('patient')
  @UseInterceptors(new DefaultResponse({}))
  createPatient(@Req() req: AppRequest, @Body() patient: PatientDto): Observable<RSP<Patient>> {
    const patientData: Patient = { ...patient };
    patientData.cabinetId = req.user.cabinetId;
    return this.patientService.createPatient(patientData);
  }
}
