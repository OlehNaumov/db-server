import { Injectable, OnModuleInit } from '@nestjs/common';
import {
  PatientGrpcService,
  Patient,
  RSP,
  PatientSearchparams,
} from 'app/interfaces';
import { Client, ClientGrpc } from '@nestjs/microservices';
import { patientGrpcOptions } from 'app/options_grpc';
import { Observable } from 'rxjs';

@Injectable()
export class PatientService implements OnModuleInit {
  @Client(patientGrpcOptions)
  private client: ClientGrpc;

  private patientGrpcService: PatientGrpcService;

  onModuleInit(): void {
    this.patientGrpcService = this.client.getService<PatientGrpcService>('PatientController');
  }

  getPatient(param: PatientSearchparams): Observable<RSP<Patient>> {
    return this.patientGrpcService.getPatient(param);
  }

  createPatient(patient: Patient): Observable<RSP<Patient>> {
    return this.patientGrpcService.createPatient(patient);
  }

  patientList(cabinetId: string): Observable<RSP<Patient[]>> {
    return this.patientGrpcService.patientsList({ cabinetId });
  }

  updatePatient(cabinetId:string, patientId: string, patientData: Patient): Observable<RSP<Patient>> {
    return this.patientGrpcService.updatePatient({ cabinetId, patientId, patientData });
  }
}
