import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { Model, Document } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Cabinet, User } from 'app/interfaces';
import { BaseError } from 'app/error';
import { CABINET_STATUS } from 'db/constants';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    @InjectModel('Cabinet')
    private readonly CabinetSchema: Model<Cabinet & Document>,
    // @InjectModel('AccessToken')
    // private readonly TokenSchema: Model<AccessToken & Document>,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromHeader('authorization'),
      ignoreExpiration: false,
      secretOrKey: 'secret',
    });
  }

  async validate(payload: User):Promise<User | void> {
    const { status } = await this.CabinetSchema.findById(payload.cabinetId);

    if (status === CABINET_STATUS.DECLINED) {
      throw new BaseError({ message: 'Payment required', status: 402 });
    }
    return payload;
  }
}
