/* eslint-disable class-methods-use-this */
import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { AppRequest, AccessToken } from 'app/interfaces';
import { config } from 'node-config-ts';

import { Document, Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class IpGuard implements CanActivate {
  constructor(
    @InjectModel('AccessToken')
    private readonly AccessTokenSchema: Model<AccessToken & Document>,
  ) { }
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const { ip, headers, user: { browser, id } }: AppRequest = context.switchToHttp().getRequest();
    const tokens = await this.AccessTokenSchema.find({ userId: id });
    const { authorization } = headers;
    const isRestrictedToken = tokens.reduce((acc, { token }) => (authorization === token) || acc, false);

    return config.CHECK_TOKEN_IP
      ? ip === browser.ip && browser.userAgent === headers['user-agent']
      : !isRestrictedToken;
  }
}
