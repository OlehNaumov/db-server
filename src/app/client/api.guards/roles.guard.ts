import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AppRequest } from 'app/interfaces';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) { }

  canActivate(context: ExecutionContext): boolean {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());

    if (!roles) { return true; }

    const request: AppRequest = context.switchToHttp().getRequest();
    const { user } = request;
    return user.roles.reduce<boolean>((acc, role) => (roles.includes(role) || acc), false);
  }
}
