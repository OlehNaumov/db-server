import { Injectable, OnModuleInit } from '@nestjs/common';
import { StatisticGrpcService, GeneralStatistic, RSP } from 'app/interfaces';
import { Client, ClientGrpc } from '@nestjs/microservices';
import { statisticGrpcOptions } from 'app/options_grpc';
import { Observable } from 'rxjs';

@Injectable()
export class StatisticService implements OnModuleInit {
  @Client(statisticGrpcOptions)
  private client: ClientGrpc;

  private statisticGrpcService: StatisticGrpcService;

  onModuleInit():void {
    this.statisticGrpcService = this.client.getService<StatisticGrpcService>('StatisticController');
  }

  getGeneralStats(cabinetId: string): Observable<RSP<GeneralStatistic>> {
    return this.statisticGrpcService.getGeneralStats({ cabinetId });
  }
}
