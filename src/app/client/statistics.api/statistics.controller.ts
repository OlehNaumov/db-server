import {
  Controller,
  Req,
  Get,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { StatisticService } from 'app/client/statistics.api/statistics.service';
import { IpGuard, RolesGuard } from 'app/client/api.guards';
import { AppRequest, GeneralStatistic, RSP } from 'app/interfaces';
import { Observable } from 'rxjs';
import { DefaultResponse } from 'app/client/response.interceptor';

@Controller()
export class StatisticController {
  constructor(private statService: StatisticService) { }

  @UseGuards(AuthGuard('jwt'), RolesGuard, IpGuard)
  @Get('statistics')
  @UseInterceptors(new DefaultResponse({}))
  getGeneralStats(@Req() req: AppRequest):Observable<RSP<GeneralStatistic>> {
    return this.statService.getGeneralStats(req.user.cabinetId);
  }
}
