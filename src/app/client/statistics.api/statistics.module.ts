import { Module } from '@nestjs/common';
import { StatisticService } from 'app/client/statistics.api/statistics.service';
import { StatisticController } from 'app/client/statistics.api/statistics.controller';
import { AccessTokenSchema } from 'db/schemas';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'AccessToken', schema: AccessTokenSchema }])],
  controllers: [StatisticController],
  providers: [StatisticService],
})

export class StatisticModule { }
