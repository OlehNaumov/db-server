import { Module } from '@nestjs/common';
import { ScheduleController } from 'app/client/schedule.api/schedule.controller';
import { ScheduleService } from 'app/client/schedule.api/schedule.service';
import { AccessTokenSchema } from 'db/schemas';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'AccessToken', schema: AccessTokenSchema }])],
  controllers: [ScheduleController],
  providers: [ScheduleService],
})
export class ScheduleModule { }
