import {
  Controller,
  Get,
  Query,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ScheduleService } from 'app/client/schedule.api/schedule.service';
import { RolesGuard, IpGuard } from 'app/client/api.guards';
import {
  AppRequest,
  ScheduleSearchParams,
  ScheduleItem,
  RSP,
} from 'app/interfaces';
import { Observable } from 'rxjs';
import { DefaultResponse } from 'app/client/response.interceptor';

@Controller()
export class ScheduleController {
  constructor(private scheduleService: ScheduleService) { }

  @UseGuards(AuthGuard('jwt'), RolesGuard, IpGuard)
  @Get('/schedule')
  @UseInterceptors(new DefaultResponse([]))
  getSchedule(@Req() req: AppRequest, @Query() search: ScheduleSearchParams): Observable<RSP<ScheduleItem[]>> {
    return this.scheduleService.getSchedule(search, req.user.cabinetId);
  }
}
