import { Injectable, OnModuleInit } from '@nestjs/common';
import { Client, ClientGrpc } from '@nestjs/microservices';
import { Observable } from 'rxjs';
import { Metadata } from '@grpc/grpc-js';
import {
  ScheduleSearchParams,
  ScheduleGrpcService,
  RSP,
  ScheduleItem,
} from 'app/interfaces';
import { scheduleGrpcOptions } from 'app/options_grpc';

@Injectable()
export class ScheduleService implements OnModuleInit {
  @Client(scheduleGrpcOptions)
  private client: ClientGrpc;

  private scheduleGrpcService: ScheduleGrpcService;

  onModuleInit():void {
    this.scheduleGrpcService = this.client.getService<ScheduleGrpcService>('ScheduleController');
  }

  getSchedule(search: ScheduleSearchParams, cabinetId: string): Observable<RSP<ScheduleItem[]>> {
    const doctorsList:string[] = typeof search.doctorsList === 'string'
      ? [search.doctorsList]
      : search.doctorsList ?? [];
    const metadata = new Metadata();
    metadata.add('cabinetId', cabinetId);
    return this.scheduleGrpcService.getSchedule({
      doctorsList,
      tsFrom: search.tsFrom,
      tsTo: search.tsTo,
    }, metadata);
  }
}
