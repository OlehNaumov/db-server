import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Req,
  SetMetadata,
  UseGuards,
  Put,
  UseInterceptors,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { DiseaseService } from 'app/client/disease.api/disease.service';
import { RolesGuard, IpGuard } from 'app/client/api.guards';
import { DiseaseDto } from 'app/dto';
import { AppRequest, Disease, RSP } from 'app/interfaces';
import { Observable } from 'rxjs';
import { USER_ROLES } from 'db/constants';
import { DefaultResponse } from 'app/client/response.interceptor';

@Controller()
export class DiseaseController {
  constructor(private diseaseService: DiseaseService) { }

  @SetMetadata('roles', [USER_ROLES.ADMIN, USER_ROLES.ROOT])
  @UseGuards(AuthGuard('jwt'), RolesGuard, IpGuard)
  @Post('/disease/:cardId')
  @UseInterceptors(new DefaultResponse({}))
  createDisease(
    @Req() req: AppRequest, @Body() disease: DiseaseDto, @Param('cardId') cardId: string,
  ):Observable<RSP<Disease>> {
    return this.diseaseService.addDisease({ cabinetId: req.user.cabinetId, cardId, ...disease });
  }

  @UseGuards(AuthGuard('jwt'), RolesGuard, IpGuard)
  @Get('/disease/:cardId')
  @UseInterceptors(new DefaultResponse([]))
  getDiseaseList(@Req() req: AppRequest, @Param('cardId') cardId: string): Observable<RSP<Disease[]>> {
    return this.diseaseService.getDiseaseList({ cardId, cabinetId: req.user.cabinetId });
  }

  @UseGuards(AuthGuard('jwt'), RolesGuard, IpGuard)
  @Put('/disease/:diseaseId')
  @UseInterceptors(new DefaultResponse({}))
  updateDisease(
    @Req() req: AppRequest, @Param('diseaseId') id: string, @Body() disease: Disease,
  ): Observable<RSP<Disease>> {
    return this.diseaseService.updateDisease(req.user.cabinetId, id, disease);
  }
}
