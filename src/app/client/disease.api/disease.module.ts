import { Module } from '@nestjs/common';
import { DiseaseController } from 'app/client/disease.api/disease.controller';
import { DiseaseService } from 'app/client/disease.api/disease.service';
import { AccessTokenSchema } from 'db/schemas';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'AccessToken', schema: AccessTokenSchema }])],
  controllers: [DiseaseController],
  providers: [DiseaseService],
})
export class DiseaseModule { }
