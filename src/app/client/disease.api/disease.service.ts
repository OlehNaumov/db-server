import { Injectable, OnModuleInit } from '@nestjs/common';
import { Client, ClientGrpc } from '@nestjs/microservices';
import { Observable } from 'rxjs';
import {
  Disease,
  RSP,
  DiseaseSearchParams,
  DiseaseGrpcService,
} from 'app/interfaces';
import { diseaseGrpcOptions } from 'app/options_grpc';
import { Metadata } from '@grpc/grpc-js';

@Injectable()
export class DiseaseService implements OnModuleInit {
  @Client(diseaseGrpcOptions)
  private client: ClientGrpc;

  private diseaseGrpcService: DiseaseGrpcService;

  onModuleInit():void {
    this.diseaseGrpcService = this.client.getService<DiseaseGrpcService>('DiseaseController');
  }

  addDisease(disease: Disease): Observable<RSP<Disease>> {
    return this.diseaseGrpcService.addDisease(disease);
  }

  getDiseaseList(search: DiseaseSearchParams): Observable<RSP<Disease[]>> {
    return this.diseaseGrpcService.getDiseaseList(search);
  }

  updateDisease(cabinetId:string, diseaseId:string, data: Disease): Observable<RSP<Disease>> {
    const metadata = new Metadata();
    metadata.add('cabinetId', cabinetId);
    metadata.add('diseaseId', diseaseId);
    return this.diseaseGrpcService.updateDisease(data, metadata);
  }
}
