import {
  Controller,
  Req,
  Post,
  Body,
  UseGuards,
  Patch,
  Get,
  UseInterceptors,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from 'app/client/auth.api/auth.service';
import { UserDto } from 'app/dto';
import {
  AppRequest,
  BrowserType,
  TokenSet,
  RSP,
  TokenRequest,
} from 'app/interfaces';
import { Observable } from 'rxjs';
import { Browser } from 'app/client/decorators';
import { IpGuard } from 'app/client/api.guards';
import { DefaultResponse } from 'app/client/response.interceptor';

@Controller()
export class AuthController {
  constructor(private authService: AuthService) { }

  @Post('login')
  @UseInterceptors(new DefaultResponse({}))
  login(@Body() loginData: UserDto, @Browser() browser: BrowserType): Observable<RSP<TokenSet>> {
    return this.authService.login(loginData, browser);
  }

  @Patch('logout')
  @UseGuards(AuthGuard('jwt'), IpGuard)
  @UseInterceptors(new DefaultResponse({}))
  logout(
    @Req() req: AppRequest, @Browser() browser: BrowserType, @Body() logoutBody: TokenRequest,
  ): Observable<RSP<string>> {
    return this.authService.logout(req.user.id, { browser, ...logoutBody });
  }

  @Get('refresh-token')
  @UseInterceptors(new DefaultResponse([]))
  refresh(@Req() req: AppRequest, @Browser() browser: BrowserType): Observable<RSP<TokenSet>> {
    const { refreshtoken, authorization: token } = req.headers;

    return this.authService.refresh({ browser, refreshtoken, token });
  }
}
