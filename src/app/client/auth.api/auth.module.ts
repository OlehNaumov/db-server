import { Module } from '@nestjs/common';
import { AuthController } from 'app/client/auth.api/auth.controller';
import { AuthService } from 'app/client/auth.api/auth.service';
import { AccessTokenSchema } from 'db/schemas';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'AccessToken', schema: AccessTokenSchema }]),
  ],
  controllers: [AuthController],
  providers: [AuthService],
})

export class AuthModule { }
