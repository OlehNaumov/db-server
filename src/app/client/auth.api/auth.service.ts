import { Injectable, OnModuleInit } from '@nestjs/common';
import {
  LoginData,
  TokenSet,
  BrowserType,
  RSP,
  TokenRequest,
  AuthGrpcService,
} from 'app/interfaces';
import { Client, ClientGrpc } from '@nestjs/microservices';
import { authGrpcOptions } from 'app/options_grpc';
import { Observable } from 'rxjs';
import { Metadata } from '@grpc/grpc-js';

@Injectable()
export class AuthService implements OnModuleInit {
  @Client(authGrpcOptions)
  private client: ClientGrpc;

  private authGrpcService: AuthGrpcService;

  onModuleInit():void {
    this.authGrpcService = this.client.getService<AuthGrpcService>('AuthController');
  }

  login(loginData: LoginData, browser: BrowserType): Observable<RSP<TokenSet>> {
    const metadata = new Metadata();
    metadata.add('browser', JSON.stringify(browser));
    return this.authGrpcService.login(loginData, metadata);
  }

  logout(
    userId:string,
    { browser, token, refreshtoken }:TokenRequest,
  ):Observable<RSP<string>> {
    const metadata = new Metadata();
    metadata.add('browser', JSON.stringify(browser));
    metadata.add('token', token);
    metadata.add('refreshtoken', refreshtoken);
    return this.authGrpcService.logout({ userId }, metadata);
  }

  refresh(data:TokenRequest):Observable<RSP<TokenSet>> {
    const metadata = new Metadata();
    metadata.add('browser', JSON.stringify(data.browser));
    return this.authGrpcService.refresh(data, metadata);
  }
}
