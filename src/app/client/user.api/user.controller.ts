import {
  Controller,
  Delete,
  Req,
  Param,
  Post,
  Get,
  Body,
  UseGuards,
  SetMetadata,
  UseInterceptors,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UserService } from 'app/client/user.api/user.service';
import { UserDto } from 'app/dto';
import {
  AppRequest,
  BrowserType,
  TokenSet,
  RSP,
  User,
} from 'app/interfaces';
import { Observable } from 'rxjs';
import { Browser } from 'app/client/decorators';
import { IpGuard, RolesGuard } from 'app/client/api.guards';
import { USER_ROLES } from 'db/constants';
import { DefaultResponse } from 'app/client/response.interceptor';

@Controller()
export class UserController {
  constructor(private userService: UserService) { }

  @Post('user')
  @UseInterceptors(new DefaultResponse({}))
  createUser(@Body() user: UserDto, @Browser() browser: BrowserType): Observable<RSP<User & TokenSet>> {
    return this.userService.createUser(user, browser);
  }

  @Get('user/list')
  @UseGuards(AuthGuard('jwt'), IpGuard)
  @UseInterceptors(new DefaultResponse([]))
  getUsersList(@Req() req: AppRequest):Observable<RSP<User[]>> {
    const { cabinetId } = req.user;
    return this.userService.getUsersList(cabinetId);
  }

  @Post('user/accept-invitation/:token')
  @UseInterceptors(new DefaultResponse({}))
  acceptInvitation(@Body() user: UserDto, @Param('token') token: string):Observable<RSP<User>> {
    return this.userService.acceptInvitation(user, token);
  }

  @Get('user')
  @UseGuards(AuthGuard('jwt'), IpGuard)
  @UseInterceptors(new DefaultResponse({}))
  getUser(@Req() req: AppRequest): Observable<RSP<User>> {
    const user_id = req.user.id;
    return this.userService.getUser(user_id);
  }

  @SetMetadata('roles', [USER_ROLES.ROOT])
  @Delete('user/:userId')
  @UseGuards(AuthGuard('jwt'), RolesGuard, IpGuard)
  @UseInterceptors(new DefaultResponse(''))
  removeUser(@Req() req: AppRequest, @Param('userId') userId: string):Observable<RSP<string>> {
    return this.userService.removeUser(userId, req.user);
  }
}
