import { Injectable, OnModuleInit } from '@nestjs/common';
import {
  UserGrpcService,
  User,
  TokenSet,
  BrowserType,
  RSP,
} from 'app/interfaces';
import { Client, ClientGrpc } from '@nestjs/microservices';
import { userGrpcOptions } from 'app/options_grpc';
import { Observable } from 'rxjs';
import { Metadata } from '@grpc/grpc-js';

@Injectable()
export class UserService implements OnModuleInit {
  @Client(userGrpcOptions)
  private client: ClientGrpc;

  private userGrpcService: UserGrpcService;

  onModuleInit():void {
    this.userGrpcService = this.client.getService<UserGrpcService>('UserController');
  }

  createUser(user: User, browser: BrowserType):Observable<RSP<User & TokenSet>> {
    const metadata = new Metadata();
    metadata.set('browser', JSON.stringify(browser));
    return this.userGrpcService.createUser(user, metadata);
  }

  getUser(_id: string): Observable<RSP<User>> {
    return this.userGrpcService.getUser({ _id });
  }

  acceptInvitation(user: User, token: string): Observable<RSP<User>> {
    const metadata = new Metadata();
    metadata.add('token', token);
    return this.userGrpcService.acceptInvitation(user, metadata);
  }

  getUsersList(cabinetId:string): Observable<RSP<User[]>> {
    return this.userGrpcService.getUsersList({ cabinetId });
  }

  removeUser(userId:string, user:User):Observable<RSP<string>> {
    const metadata = new Metadata();
    metadata.add('user', JSON.stringify(user));
    return this.userGrpcService.removeUser({ _id: userId }, metadata);
  }
}
