import { Module } from '@nestjs/common';
import { UserController } from 'app/client/user.api/user.controller';
import { UserService } from 'app/client/user.api/user.service';
import { AccessTokenSchema } from 'db/schemas';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'AccessToken', schema: AccessTokenSchema }]),
  ],
  controllers: [UserController],
  providers: [UserService],
})

export class UserModule { }
