import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { BrowserType } from 'app/interfaces';

export const Browser = createParamDecorator(
  (data: unknown, ctx: ExecutionContext): BrowserType => {
    const request = ctx.switchToHttp().getRequest();

    const { ip, headers } = request;

    return { ip, userAgent: headers['user-agent'] };
  },
);
