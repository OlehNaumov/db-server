import { Module } from '@nestjs/common';
import { ClientAppModule } from 'app/client/app.module';
import { ServerAppModule } from 'app/server/app.module';

@Module({
  imports: [ClientAppModule, ServerAppModule],
  controllers: [],
  providers: [],
})

export class AppModule { }
