import { config } from 'node-config-ts';
import { Transport, ClientOptions } from '@nestjs/microservices';
import { join } from 'path';

const { GRPC } = config;

type options = {
  url: string;
  package: string;
  protoPath: string;
};

type GrpcOptions = {
  transport: number;
  options: options;
};

const getOptions = (packageName: string): GrpcOptions => ({
  transport: Transport.GRPC,
  options: {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    url: GRPC[packageName].url,
    package: packageName,
    protoPath: join(__dirname, `../protoes/${packageName}.proto`),
  },
});

export const userGrpcOptions: ClientOptions = getOptions('user');
export const cabinetGrpcOptions: ClientOptions = getOptions('cabinet');
export const patientGrpcOptions: ClientOptions = getOptions('patient');
export const cardGrpcOptions: ClientOptions = getOptions('card');
export const diseaseGrpcOptions: ClientOptions = getOptions('disease');
export const chartGrpcOptions: ClientOptions = getOptions('chart');
export const scheduleGrpcOptions: ClientOptions = getOptions('schedule');
export const periodentalGrpcOptions: ClientOptions = getOptions('periodental');
export const statisticGrpcOptions: ClientOptions = getOptions('statistic');
export const authGrpcOptions: ClientOptions = getOptions('auth');
