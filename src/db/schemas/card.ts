import { Schema, Types } from 'mongoose';
import { CardDb } from 'db/dbInterfaces';

const CardSchema = new Schema<CardDb>({
  patientId: { type: Types.ObjectId, ref: 'Patient' },
  cabinetId: { type: Types.ObjectId, ref: 'Cabinet' },
  notes: String,
}, { timestamps: true });

CardSchema.index({ patientId: 1 });
CardSchema.index({ cabinetId: 1 });

export { CardSchema };
