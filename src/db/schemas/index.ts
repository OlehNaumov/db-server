import { CabinetSchema } from 'db/schemas/cabinet';
import { CardSchema } from 'db/schemas/card';
import { ChartSchema } from 'db/schemas/chart';
import { PatientSchema } from 'db/schemas/patient';
import { UserSchema } from 'db/schemas/user';
import { PeriodentalChartSchema } from 'db/schemas/periodental';
import { AccessTokenSchema } from 'db/schemas/accessTokens';
import { DiseaseSchema } from 'db/schemas/disease';

export {
  UserSchema,
  CabinetSchema,
  PatientSchema,
  CardSchema,
  ChartSchema,
  PeriodentalChartSchema,
  AccessTokenSchema,
  DiseaseSchema,
};
