import { Schema, Types, Document } from 'mongoose';
import { PeriodentalTooth, UpPeriodental, BottomPeriodental } from 'app/interfaces';
import { PeriodentalChartDb } from 'db/dbInterfaces';

const default0 = {
  type: Number,
  default: 0,
};

const defaultFalse = {
  type: Boolean,
  default: false,
};

const PeriodentalToothSchema = new Schema<PeriodentalTooth & Document>({
  absent: defaultFalse,
  mobility: default0,
  implant: defaultFalse,
  furcation: default0,
  bleeding: {
    v1: defaultFalse,
    v2: defaultFalse,
    v3: defaultFalse,
  },
  pluc: default0,
  margin: {
    v1: default0,
    v2: default0,
    v3: default0,
  },
  depth: {
    v1: default0,
    v2: default0,
    v3: default0,
  },
});

const upScheme = new Schema<UpPeriodental & Document>({
  t18: { type: PeriodentalToothSchema, default: {} },
  t17: { type: PeriodentalToothSchema, default: {} },
  t16: { type: PeriodentalToothSchema, default: {} },
  t15: { type: PeriodentalToothSchema, default: {} },
  t14: { type: PeriodentalToothSchema, default: {} },
  t13: { type: PeriodentalToothSchema, default: {} },
  t12: { type: PeriodentalToothSchema, default: {} },
  t11: { type: PeriodentalToothSchema, default: {} },
  t28: { type: PeriodentalToothSchema, default: {} },
  t27: { type: PeriodentalToothSchema, default: {} },
  t26: { type: PeriodentalToothSchema, default: {} },
  t25: { type: PeriodentalToothSchema, default: {} },
  t24: { type: PeriodentalToothSchema, default: {} },
  t23: { type: PeriodentalToothSchema, default: {} },
  t22: { type: PeriodentalToothSchema, default: {} },
  t21: { type: PeriodentalToothSchema, default: {} },
});

const bottomScheme = new Schema<BottomPeriodental & Document>({
  t48: { type: PeriodentalToothSchema, default: {} },
  t47: { type: PeriodentalToothSchema, default: {} },
  t46: { type: PeriodentalToothSchema, default: {} },
  t45: { type: PeriodentalToothSchema, default: {} },
  t44: { type: PeriodentalToothSchema, default: {} },
  t43: { type: PeriodentalToothSchema, default: {} },
  t42: { type: PeriodentalToothSchema, default: {} },
  t41: { type: PeriodentalToothSchema, default: {} },
  t38: { type: PeriodentalToothSchema, default: {} },
  t37: { type: PeriodentalToothSchema, default: {} },
  t36: { type: PeriodentalToothSchema, default: {} },
  t35: { type: PeriodentalToothSchema, default: {} },
  t34: { type: PeriodentalToothSchema, default: {} },
  t33: { type: PeriodentalToothSchema, default: {} },
  t32: { type: PeriodentalToothSchema, default: {} },
  t31: { type: PeriodentalToothSchema, default: {} },
});

const PeriodentalChartSchema = new Schema<PeriodentalChartDb>({
  patientId: { type: Types.ObjectId, ref: 'Patient' },
  chart: {
    up: {
      buccal: upScheme,
      lingual: upScheme,
    },
    down: {
      buccal: bottomScheme,
      lingual: bottomScheme,
    },
  },
});

export { PeriodentalChartSchema };
