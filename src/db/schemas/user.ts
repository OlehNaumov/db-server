import * as m from 'moment';
import { Schema, Types } from 'mongoose';
import { hashSync, compareSync } from 'bcryptjs';
import { UserDb } from 'db/dbInterfaces';

const USER_ROLES = Object.freeze({
  ADMIN: 'ADMIN',
  ROOT: 'ROOT',
  DOCTOR: 'DOCTOR',
});

const UserSchema = new Schema<UserDb>({
  name: String,
  surname: String,
  secondname: String,
  hashedPassword: String,
  phone: String,
  email: {
    type: String,
    unique: true,
  },
  createdAt: { type: Number, default: m().unix() },
  roles: [{
    type: String,
    required: true,
    enum: Object.values(USER_ROLES),
  }],
  cabinetsCount: { type: Number, default: 0 },
  cabinetId: { type: Types.ObjectId, ref: 'Cabinet', index: true },
});

UserSchema.virtual('password')
  // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
  .set(function setPassword(password:string): void { this.hashedPassword = hashSync(password); })
  // eslint-disable-next-line @typescript-eslint/no-unsafe-return, @typescript-eslint/no-unsafe-member-access
  .get(function getPassword():string { return this.hashedPassword; });

UserSchema.methods.comparePasswords = function comparePasswords(password): boolean {
  // eslint-disable-next-line @typescript-eslint/no-unsafe-return
  return compareSync(password, this.hashedPassword);
};

UserSchema.methods.addCabinet = async function addCabinet(): Promise<UserDb> {
  this.cabinetsCount += 1;
  return this.save();
};

UserSchema.methods.removeCabinet = async function removeCabinet(): Promise<UserDb> {
  this.cabinetsCount = 1;
  return this.save();
};

export { UserSchema, USER_ROLES };
