import * as m from 'moment';
import { Schema, Types } from 'mongoose';
import { PatientDb } from 'db/dbInterfaces';

const PatientSchema = new Schema<PatientDb>({
  name: String,
  surname: String,
  secondname: String,
  email: String,
  dob: Date,
  address: String,
  phone: String,
  cardId: { type: Types.ObjectId, ref: 'Card' },
  periodentalChartId: { type: Types.ObjectId, ref: 'PeriodentalChart' },
  toothFormulaId: { type: Types.ObjectId, ref: 'Formula' },
  image: String,
  createdAt: { type: Number, default: m().unix() },
  cabinetId: { type: Types.ObjectId, ref: 'Cabinet' },
});

PatientSchema.index({ cabinetId: 1 });

export { PatientSchema };
