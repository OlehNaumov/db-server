import { Schema, Types } from 'mongoose';
import { DiseaseDb } from 'db/dbInterfaces';

const DiseaseSchema = new Schema<DiseaseDb>({
  dateFrom: { type: Number, required: true },
  dateTo: { type: Number, required: true },
  name: String,
  diagnosis: String,
  price: Number,
  payed: { type: Number, default: 0 },
  materials: String,
  description: String,
  doctorId: { type: Types.ObjectId, ref: 'User', required: true },
  cardId: { type: Types.ObjectId, ref: 'Card', required: true },
  cabinetId: { type: Types.ObjectId, ref: 'Cabinet', required: true },
}, { timestamps: true });

DiseaseSchema.index({ doctorId: 1 });
DiseaseSchema.index({ dateFrom: 1 });
DiseaseSchema.index({ dateTo: 1 });
DiseaseSchema.index({ cardId: 1 });
DiseaseSchema.index({ cabinetId: 1 });

export { DiseaseSchema };
