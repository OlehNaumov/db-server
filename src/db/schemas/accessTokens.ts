import { Schema, Types } from 'mongoose';
import { config } from 'node-config-ts';
import { AccessTokenDb } from 'db/dbInterfaces';
import * as moment from 'moment';

const { JWT: { expire: expires } } = config;

const AccessTokenSchema = new Schema<AccessTokenDb>({
  userId: { type: Types.ObjectId, ref: 'User' },
  token: String,
  refreshtoken: String,
  ip: String,
  createdAt: { type: Date, default: () => moment() },
});

AccessTokenSchema.index({ userId: 1 }, { expires });

export { AccessTokenSchema };
