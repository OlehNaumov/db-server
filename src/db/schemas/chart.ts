import { ChartDb } from 'db/dbInterfaces';
import * as m from 'moment';
import { Schema, Types } from 'mongoose';

const ZONE_COLORS = Object.freeze({
  red: '#d93107',
  yellow: '#debb10',
  green: '#6dd611',
  blue: '#0d82db',
  purple: '#c410c7',
  default: '#0000',
});

const zone = { type: Boolean, default: false };
const zoneParam = { type: Boolean, default: false, required: false };

const getArray = (count: number):number[] => {
  const arr: number[] = [];
  for (let i = 1; i <= count; i++) { arr.push(i); }
  return arr;
};

// eslint-disable-next-line @typescript-eslint/no-unsafe-return
const getZonesColors = (count: number) => getArray(count).reduce((accum, zoneNumber) => ({
  [`c${zoneNumber}`]: { type: String, default: ZONE_COLORS.default, enum: Object.values(ZONE_COLORS) },
  ...accum,
}), {});

// console.log(getZonesColors(4));

const FiveZoneToothScheme = new Schema<any>({
  zone: { z1: zone, z2: zone, z3: zone, z4: zone, z5: zone },
  removed: zoneParam,
  implant: zoneParam,
  crown: zoneParam,
  description: { type: String, default: '' },
  colors: getZonesColors(4),
}, { _id: false });

const FourZoneToothSchema = new Schema<any>({
  zone: { z1: zone, z2: zone, z3: zone, z4: zone },
  removed: zoneParam,
  implant: zoneParam,
  crown: zoneParam,
  description: { type: 'String', default: '' },
  colors: getZonesColors(2),
}, { _id: false });

const ChartSchema = new Schema<ChartDb>({
  patientId: { type: Types.ObjectId, ref: 'Patient' },
  createdAt: { type: Number, default: m().unix() },
  notes: String,
  chart: {
    t18: { type: FiveZoneToothScheme, default: {} },
    t17: { type: FiveZoneToothScheme, default: {} },
    t16: { type: FiveZoneToothScheme, default: {} },
    t28: { type: FiveZoneToothScheme, default: {} },
    t27: { type: FiveZoneToothScheme, default: {} },
    t26: { type: FiveZoneToothScheme, default: {} },
    t38: { type: FiveZoneToothScheme, default: {} },
    t37: { type: FiveZoneToothScheme, default: {} },
    t36: { type: FiveZoneToothScheme, default: {} },
    t48: { type: FiveZoneToothScheme, default: {} },
    t47: { type: FiveZoneToothScheme, default: {} },
    t46: { type: FiveZoneToothScheme, default: {} },
    t15: { type: FourZoneToothSchema, default: {} },
    t14: { type: FourZoneToothSchema, default: {} },
    t13: { type: FourZoneToothSchema, default: {} },
    t12: { type: FourZoneToothSchema, default: {} },
    t11: { type: FourZoneToothSchema, default: {} },
    t25: { type: FourZoneToothSchema, default: {} },
    t24: { type: FourZoneToothSchema, default: {} },
    t23: { type: FourZoneToothSchema, default: {} },
    t22: { type: FourZoneToothSchema, default: {} },
    t21: { type: FourZoneToothSchema, default: {} },
    t35: { type: FourZoneToothSchema, default: {} },
    t34: { type: FourZoneToothSchema, default: {} },
    t33: { type: FourZoneToothSchema, default: {} },
    t32: { type: FourZoneToothSchema, default: {} },
    t31: { type: FourZoneToothSchema, default: {} },
    t45: { type: FourZoneToothSchema, default: {} },
    t44: { type: FourZoneToothSchema, default: {} },
    t43: { type: FourZoneToothSchema, default: {} },
    t42: { type: FourZoneToothSchema, default: {} },
    t41: { type: FourZoneToothSchema, default: {} },
  },
});

export { ChartSchema, ZONE_COLORS };
