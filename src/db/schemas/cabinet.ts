import * as m from 'moment';
import { Schema, Types } from 'mongoose';
import { CabinetDb } from 'db/dbInterfaces';

const HOURS_REGEXP = /^[0-9]{2}\:[0-9]{2}$/;

export const CABINET_STATUS = Object.freeze({
  ACTIVE: 'ACTIVE',
  PENDING: 'PENDING',
  DECLINED: 'DECLINED',
});

const CabinetSchema = new Schema<CabinetDb>({
  name: { type: String },
  address: String,
  phone: String,
  logo: String,
  additionalInfo: String,
  workHoursFrom: {
    type: String,
    default: '08:00',
    validate: {
      validator: function hoursValidator() {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
        return HOURS_REGEXP.test(this.workHoursFrom);
      },
      message: 'Invalid cabinet working time',
    },
  },
  workHoursTo: {
    type: String,
    default: '20:00',
    validate: {
      validator: function hoursValidator() {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
        return HOURS_REGEXP.test(this.workHoursTo);
      },
      message: 'Invalid cabinet working time',
    },
  },
  createdAt: { type: Number, default: m().unix() },
  users: [{ type: Types.ObjectId, ref: 'User' }],
  usedInvitations: [String],
  status: {
    type: String,
    default: CABINET_STATUS.ACTIVE,
    enum: Object.values(CABINET_STATUS),
    required: true,
  },
});

CabinetSchema.methods.inviteUser = async function inviteUser(
  user: Types.ObjectId | string,
  token: string,
) {
  const canUseToken = this.usedInvitations.indexOf(token) < 0;
  if (canUseToken) {
    this.users.push(user);
    this.usedInvitations.push(token);
    return this.save();
  }
  throw new Error('Invitation token already used');
};

export { CabinetSchema };
