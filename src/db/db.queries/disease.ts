import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Disease, ScheduleSearchParams, ScheduleItem } from 'app/interfaces';
import { Document, Model, Types } from 'mongoose';
import { BaseError } from 'app/error';

@Injectable()
export class DiseaseDb {
  constructor(
    @InjectModel('Disease') private readonly DiseaseModel: Model<Disease & Document>,
  ) { }

  public async addDisease(diseaseData: Disease): Promise<Disease> {
    const disease = new this.DiseaseModel(diseaseData);
    return disease.save();
  }

  public async getDiseaseList(cardId: string): Promise<Disease[]> {
    return this.DiseaseModel.find({ cardId });
  }
  public async updateDisease(cabinetId:string, _id:string, diseaseData: Disease): Promise<Disease> {
    const disease = await this.DiseaseModel.findOne({ cabinetId, _id })
      .orFail(new BaseError({ message: 'Disease not found', status: 404 }));

    return disease.set(diseaseData).save();
  }
  public async getDiseasesWitFilter(cabinetId: string, filter: ScheduleSearchParams): Promise<ScheduleItem[]> {
    const andQuery: any[] = [
      { dateFrom: { $gte: filter.tsFrom } },
      { dateFrom: { $lte: filter.tsTo } },
    ];

    if (filter.doctorsList) {
      andQuery.push({ doctorId: { $in: filter.doctorsList.map(id => Types.ObjectId(id)) } });
    }

    return this.DiseaseModel.aggregate()
      .match({
        cabinetId: Types.ObjectId(cabinetId),
        $and: andQuery,
      })
      .lookup({
        from: 'cards',
        as: 'card',
        foreignField: '_id',
        localField: 'cardId',
      })
      .unwind('card')
      .lookup({
        from: 'patients',
        as: 'patient',
        foreignField: '_id',
        localField: 'card.patientId',
      })
      .unwind('patient')
      .lookup(
        {
          from: 'users',
          as: 'doctor',
          foreignField: '_id',
          localField: 'doctorId',
        },
      )
      .unwind('doctor')
      .project({
        id: '$_id',
        _id: 0,
        doctorName: '$doctor.name',
        doctorSurname: '$doctor.surname',
        doctorSecondName: '$doctor.secondname',
        doctorId: '$doctor._id',
        patientName: '$patient.name',
        patientSurname: '$patient.surname',
        patientSecondName: '$patient.secondname',
        patientId: '$patient._id',
        dateFrom: '$dateFrom',
        dateTo: '$dateTo',
      });
  }
}
