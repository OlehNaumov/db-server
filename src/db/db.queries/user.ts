import { Injectable } from '@nestjs/common';
import { Model, Document, isValidObjectId } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserSearchParam } from 'app/interfaces';

@Injectable()
export class UserDb {
  constructor(
    @InjectModel('User') private readonly UserScheme: Model<User & Document>,
  ) { }

  public async getUser(searchParam: UserSearchParam): Promise<User & Document> {
    const $or = [];

    searchParam.email && $or.push({ email: searchParam.email });
    isValidObjectId(searchParam._id) && $or.push({ _id: searchParam._id });

    return this.UserScheme.findOne({ $or });
  }

  public getUserModel(userData: User): User & Document {
    return new this.UserScheme(userData);
  }

  public createUser(userData: User): Promise<User & Document> {
    return new this.UserScheme(userData).save();
  }

  public async getUsersAmountByCabinetId(cabinetId: string): Promise<number> {
    return this.UserScheme.countDocuments({ cabinetId });
  }

  public async getUsersList(cabinetId: string): Promise<User[]> {
    return this.UserScheme.find({ cabinetId })
      .select('email name surname secondname createdAt roles dob phone');
  }

  public async removeUser(userId: string): Promise<User> {
    return this.UserScheme.findByIdAndDelete(userId);
  }
}
