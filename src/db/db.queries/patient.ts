import { Injectable } from '@nestjs/common';
import { Model, Document } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Patient } from 'app/interfaces';

@Injectable()
export class PatientDb {
  constructor(
    @InjectModel('Patient')
    private readonly PatientScheme: Model<Patient & Document>,
  ) {}

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public async getPatient(params: any): Promise<Patient> {
    return this.PatientScheme.findOne(params);
  }

  public createPatient(patientData: Patient): Promise<Patient> {
    const patient = new this.PatientScheme(patientData);
    return patient.save();
  }

  public getNewPatientModel(patientData: Patient): Patient & Document {
    return new this.PatientScheme(patientData);
  }

  public async patientsList(cabinetId: string): Promise<Patient[]> {
    return this.PatientScheme.find({ cabinetId });
  }

  public async updatePatientById(patientId: string, data: Patient): Promise<Patient> {
    return this.PatientScheme.findOneAndUpdate({ _id: patientId }, data, { new: true });
  }

  public async getPatientsAmountByCabinetId(cabinetId: string): Promise<number> {
    return this.PatientScheme.countDocuments({ cabinetId });
  }
}
