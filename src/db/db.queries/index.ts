import { CabinetDb } from 'db/db.queries/cabinet';
import { CardDb } from 'db/db.queries/card';
import { ChartDb } from 'db/db.queries/chart';
import { PatientDb } from 'db/db.queries/patient';
import { UserDb } from 'db/db.queries/user';
import { PeriodentalChartDb } from 'db/db.queries/periodental';
import { TokenDb } from 'db/db.queries/tokens';
import { DiseaseDb } from 'db/db.queries/disease';

export {
  UserDb,
  CabinetDb,
  PatientDb,
  CardDb,
  ChartDb,
  PeriodentalChartDb,
  TokenDb,
  DiseaseDb,
};
