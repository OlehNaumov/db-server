import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Card, CardSearchParam, Disease } from 'app/interfaces';
import { Document, Model, Types } from 'mongoose';
import { BaseError } from 'app/error';

@Injectable()
export class CardDb {
  constructor(
    @InjectModel('Card') private readonly CardModel: Model<Card & Document>,
  ) { }

  public async getCard(searchParam: CardSearchParam): Promise<Card> {
    return this.CardModel.findOne(searchParam)
      .orFail(new BaseError({ message: 'Card not found', status: 404 }));
  }

  public async addDisease(cardId: string, diseases: Disease): Promise<Card> {
    return this.CardModel.findOneAndUpdate(
      { _id: cardId },
      { $push: { diseases } },
      { new: true },
    );
  }

  public async createCard(cardData: Card): Promise<Card> {
    return new this.CardModel(cardData).save();
  }

  public async getPatientsIdsInCabinet(cabinetId: string): Promise<{ patients:string[] }[]> {
    return this.CardModel.aggregate([
      { $match: { cabinetId: Types.ObjectId(cabinetId) } },
      {
        $group: {
          _id: '$cabinetId',
          patients: { $push: '$patientId' },
        },
      },
    ]);
  }
}
