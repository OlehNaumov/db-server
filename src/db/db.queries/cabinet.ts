import { Injectable } from '@nestjs/common';
import { Model, Document } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Cabinet } from 'app/interfaces';

@Injectable()
export class CabinetDb {
  constructor(
    @InjectModel('Cabinet')
    private readonly CabinetScheme: Model<Cabinet & Document>,
  ) {}

  public async getCabinet(userId: string): Promise<Cabinet | null> {
    return this.CabinetScheme.findOne({ users: userId });
  }

  public async getCabinetById(id: string): Promise<Cabinet | null> {
    return this.CabinetScheme.findById(id);
  }

  public async updateCabinetById(id:string, cabinetData:Cabinet): Promise<Cabinet | null> {
    return this.CabinetScheme.findByIdAndUpdate(id, { $set: cabinetData }, { new: true });
  }

  public createCabinet(cabinetData: Cabinet): Promise<Cabinet> {
    const cabinet = new this.CabinetScheme(cabinetData);
    return cabinet.save();
  }
}
