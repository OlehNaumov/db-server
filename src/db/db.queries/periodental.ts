import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PeriodentalChartData } from 'app/interfaces';
import { Document, Model } from 'mongoose';

@Injectable()
export class PeriodentalChartDb {
  constructor(
    @InjectModel('Periodental')
    private readonly PeriodentalChartScheme: Model<PeriodentalChartData & Document>,
  ) { }

  public createPeriodentalChart(patientId:string): Promise<PeriodentalChartData> {
    const chart = {
      up: { buccal: {}, lingual: {} },
      down: { buccal: {}, lingual: {} },
    };
    return new this.PeriodentalChartScheme({ patientId, chart }).save();
  }

  public async getPeriodentalChart(patientId:string):Promise<PeriodentalChartData> {
    return this.PeriodentalChartScheme.findOne({ patientId });
  }
}
