import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { AccessTokenDb } from 'db/dbInterfaces';
import { AccessToken } from 'app/interfaces';

@Injectable()
export class TokenDb {
  constructor(
    @InjectModel('AccessToken') private readonly TokenSchema: Model<AccessTokenDb>,
  ) { }

  public async getUserModel(userId: string): Promise<AccessTokenDb> {
    return this.TokenSchema.findOne({ userId });
  }
  public async restrictToken(data: AccessToken): Promise<AccessTokenDb> {
    return this.TokenSchema.create(data);
  }
}
