import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Chart } from 'app/interfaces';
import { Document, Model } from 'mongoose';

@Injectable()
export class ChartDb {
  constructor(@InjectModel('Chart') private readonly ChartScheme: Model<Chart & Document>) { }

  public createChart(chartData: Chart): Promise<Chart> { return new this.ChartScheme(chartData).save(); }

  public async getChart(_id:string, patientIds:string[]): Promise<Chart & Document> {
    return this.ChartScheme.findOne({ _id, patientId: { $in: patientIds } });
  }
}
