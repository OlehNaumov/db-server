import { config } from 'node-config-ts';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    MongooseModule.forRoot(config.DB.mongoose.url, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      createIndexes: true,
      useFindAndModify: false,
    }),
  ],
})
export class DbModule {}
