import { Document, Types } from 'mongoose';
import { PeriodentalChart } from 'app/interfaces';

export interface CabinetDb extends Document {
  name: string;
  address: string;
  phone: string;
  logo: string;
  additionalInfo: string;
  createdAt: number;
  users: [Types.ObjectId | string];
  usedInvitations: [string];
  workHoursFrom: string;
  workHoursTo: string;
  status: string;
}

export interface DiseaseDb extends Document {
  createdAt: number;
  date: number;
  name: string;
  diagnosis: string;
  price: number;
  payed: number;
  materials: string;
  description: string;
  doctorId: Types.ObjectId;
}

export interface CardDb extends Document {
  patientId: Types.ObjectId;
  cabinetId: Types.ObjectId;
  createdAt: Types.ObjectId;
  notes: string;
  diseases: DiseaseDb[];
}

export interface PatientDb extends Document {
  name: string;
  surname: string;
  secondname: string;
  email: string;
  dob: Date;
  address: string;
  phone: string;
  cardId: Types.ObjectId;
  periodentalChartId: Types.ObjectId;
  toothFormulaId: Types.ObjectId;
  image: string;
  createdAt: number;
  cabinetId: Types.ObjectId;
}

export interface UserDb extends Document {
  name: string;
  surname: string;
  secondname: string;
  hashedPassword: string;
  email: {
    type: string;
    unique: true;
  };
  createdAt: number;
  roles: [];
  cabinetsCount: number;
  cabinetId: Types.ObjectId;
}

export interface ChartDb extends Document {
  patientId: { type: Types.ObjectId },
  createdAt: number,
  notes: string,
  chart: any
}

export interface PeriodentalChartDb extends Document {
  patientId: Types.ObjectId,
  chart: PeriodentalChart
}

export interface AccessTokenDb extends Document {
  userId: Types.ObjectId,
  token: string,
  refreshtoken: string,
  ip: string,
}
