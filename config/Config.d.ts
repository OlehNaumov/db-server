/* tslint:disable */
/* eslint-disable */
declare module "node-config-ts" {
  interface IConfig {
    GRPC: GRPC
    DB: DB
    PORT: number
    PORT_SET: number[]
    JWT: JWT
    UNLIMITED_JWT: UNLIMITEDJWT
    crypto: Crypto
    USE: string
    CHECK_TOKEN_IP: boolean
  }
  interface Crypto {
    invitation: string
  }
  interface UNLIMITEDJWT {
    secret: string
    verifyOptions: VerifyOptions
    signOptions: SignOptions
  }
  interface SignOptions {
    algorithm: string
  }
  interface VerifyOptions {
    algorithms: string[]
  }
  interface JWT {
    algorithms: string[]
    secret: string
    algorithm: string
    expire: string
  }
  interface DB {
    mongoose: User
  }
  interface GRPC {
    user: User
    cabinet: User
    patient: User
    card: User
    disease: User
    chart: User
    schedule: User
    periodental: User
    statistic: User
    auth: User
  }
  interface User {
    url: string
  }
  export const config: Config
  export type Config = IConfig
}
