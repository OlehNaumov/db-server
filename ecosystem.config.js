module.exports = {
  apps: [{
    name: 'dental-box',
    script: './dist/main.js',
    instances: 1,
    // exec_mode: "cluster",
    watch: true,
    // increment_var: 'PORT',
    env: {
      PORT: 3000,
      NODE_ENV: 'development',
    },
  }],
};
