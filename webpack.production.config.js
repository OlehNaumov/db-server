// const path = require('path');
const { TsconfigPathsPlugin } = require('tsconfig-paths-webpack-plugin');
const webpack = require('webpack');
const path = require('path');
const nodeExternals = require('webpack-node-externals');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

const isProduction = process.env.NODE_ENV === 'production';
const mode = isProduction ? 'production' : 'development';
const devtool = isProduction ? false : 'inline-source-map';

const { NodeConfigTSPlugin } = require('node-config-ts/webpack');
const CopyPlugin = require('copy-webpack-plugin');

console.log('WEBPACK ENV', mode);

const WebPackIgnorePlugin = {
  checkResource: (resource) => {
    const lazyImports = [
      // '@nestjs/microservices',
      // '@nestjs/microservices/microservices-module',
      'cache-manager',
      // 'class-transformer',
      // 'class-validator',
      // 'fastify-static',
      'amqp-connection-manager',
      'amqplib',
      'nats',
      'mqtt',
      'kafkajs',
      '@nestjs/websockets/socket-module',
    ];

    if (!lazyImports.includes(resource)) return false;

    try {
      require.resolve(resource);
    } catch (err) {
      return true;
    }

    return false;
  },
};

const Options = NodeConfigTSPlugin({
  entry: [
    'webpack/hot/poll?100',
    './src/main.ts',
  ],
  optimization: { minimize: false },
  target: 'node',
  mode,
  devtool,
  // externals: [
  //   '@grpc/proto-loader',
  //   nodeExternals({
  //     allowlist: ['webpack/hot/poll?100', './src/main.ts', './src/protoes'],
  //     modulesFromFile: {
  //       fileName: './package.json',
  //       includeInBundle: ['dependencies'],
  //     },
  //   }),
  // ],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'awesome-typescript-loader',
        options: { transpileOnly: true },
        exclude: /node_modules/,
      },
      {
        test: /\.(json|)$/,
        type: 'json',
        include: /config/,
      },
      {
        test: /load-package\.util\.js$/,
        loader: 'string-replace-loader',
        options: {
          search: 'require[(]([^\'"])',
          replace: '__non_webpack_require__($1',
          flags: 'g',
        },
      },
      {
        test: /app\.js$/,
        loader: 'string-replace-loader',
        options: {
          replace: 'require[(]([^\'"])',
          search: '__non_webpack_require__($1',
          flags: 'g',
        },
      },
    ],
  },
  plugins: [
    new webpack.IgnorePlugin(WebPackIgnorePlugin),
    new webpack.HotModuleReplacementPlugin(),
    // new webpack.WatchIgnorePlugin({ paths: [/\.js$/, /\.d\.ts$/] }),
    new ForkTsCheckerWebpackPlugin({ typescript: true }),
    new webpack.DefinePlugin({ CONFIG: JSON.stringify(require('node-config-ts')) }),
    // new RunScriptWebpackPlugin({ name: 'server.js' }),
    new webpack.ContextReplacementPlugin(/.*/),
    new CopyPlugin({ patterns: [{ from: './src/protoes', to: './protoes' }] }),
  ],
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.proto'],
    plugins: [new TsconfigPathsPlugin({ configFile: './tsconfig.json' })],
    // alias: { 'app/*': './dist/app/*' },
  },
  output: {
    path: path.join(__dirname, 'app'),
    filename: 'server/app.js',
  },
});

module.exports = Options;
